<?php

namespace App\Contracts;

use App\Models\File;
use Illuminate\Http\UploadedFile;

interface UploadFileContract
{
    public function handle(UploadedFile $file):File;
}
