<?php

namespace App\Models;

use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Modules\Organizacion\Entities\Colaborador;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable, SoftDeletes;
    use TwoFactorAuthenticatable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        "name",
        "username",
        "email",
        "password",
        "colaborador_id",
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        "password",
        "remember_token",
        "two_factor_recovery_codes",
        "two_factor_secret",
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        "email_verified_at" => "datetime",
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ["profile_photo_url"];

    public function getStatusAttribute()
    {
        if (isset($this->deleted_at)) {
            return "inactivo";
        } elseif (is_null($this->email_verified_at)) {
            return "no confirmado";
        }
        return "activo";
    }

    public function getStatusColorAttribute()
    {
        if (isset($this->deleted_at)) {
            return "danger";
        } elseif (is_null($this->email_verified_at)) {
            return "warning";
        }
        return "success";
    }

    /**
     * Obtener el colaborador principal que tiene
     */
    public function colaborador()
    {
        return $this->belongsTo(Colaborador::class);
    }
}
