<?php

namespace App\Models;

use Modules\Sam\Entities\Historico;
use Illuminate\Database\Eloquent\Model;
use Modules\Organizacion\Entities\Colaborador;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Revision extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $table = "revisiones";

    //constantes

    const WAITING = 4; //esperando que se aprueben otras
    const CONFIRMED = 2;
    const REJECTED = 3;
    const PENDING = 1;
    const CANCELLED = 0;

    const ESTADOS = [
        self::CANCELLED => "Cancelada",
        self::PENDING => "En revisión",
        self::CONFIRMED => "Aprobado",
        self::REJECTED => "Rechazado",
        self::WAITING => "En espera",
    ];

    /**
     * Obtener el historico
     */
    public function historico()
    {
        return $this->belongsTo(Historico::class, "revisable_id");
    }

    public function revisor()
    {
        return $this->belongsTo(Colaborador::class, "colaborador_id");
    }

    public function siguiente()
    {
        return $this->belongsTo(Revision::class, "revision_siguiente_id");
    }

    public function getStatusAttribute()
    {
        return array_values(self::ESTADOS)[intval($this->estado)];
    }
}
