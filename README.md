# Sistema modular Odín para Secretaría Técnica

Demo: [http://st.rpoot.com/](http://st.rpoot.com/)

### Instalación y configuración de ambiente de desarrollo
El codigo puede ser instalado directamente desde el repositorio

#### Requisitos previos

1. Nodejs 16+
2. PHP 8.1+
3. Composer 2+
4. Servidor base de datos MySQL 8+ o MariaDB 10+
2. Git
#### 1) Clonar repositorio

```
git clone https://gitlab.com/secretaria.tecnica.informatica01/odin.git
cd odin
```
#### 2) Creación de archivo de configuraciones
El proyecto incluye un archivo base llamado .env.example, para ser utilizado solo es necesario cambiar los valores según la configuración requerida.
```
cp .env.example .env
```


#### 3) Instalación de dependencias de Nodejs
```
npm install 
```
#### 4) Instalación de dependencias de PHP
```
composer install
```
#### 5) Transpilación de SASS y JS
```
npm run dev
```
#### 6) Ejecución del servidor de PHP
```
php artisan serve
```

#### 7) Ejecución de migraciones de base de datos
```
php artisan migrate
```
#### 8) Acceder a traves de un navegador web

``
http://localhost:8000
``


### Instalación y configuración de ambiente de produccion
El codigo puede ser instalado directamente desde el repositorio

#### Requisitos previos

1. Nodejs 16+
2. PHP 8.1+
3. Composer 2+
4. Apache o Nginx (Recomendado)
4. Servidor base de datos MySQL 8+ o MariaDB 10+
2. Git
#### 1) Clonar repositorio
El repositorio debe ser clonado en la carpeta que se considere, dependiendo la configuracion de servidor que sea elegida.
```
git clone https://gitlab.com/secretaria.tecnica.informatica01/odin.git
cd odin
```
#### 2) Creación de archivo de configuraciones
El proyecto incluye un archivo base llamado .env.example, para ser utilizado solo es necesario cambiar los valores según la configuración requerida.
```
cp .env.example .env
```


#### 3) Instalación de dependencias de Nodejs
```
npm install 
```
#### 4) Instalación de dependencias de PHP
```
composer install
```
#### 5) Transpilación de SASS y JS en modo producción
```
npm run build
```
#### 6) Ejecución de migraciones de base de datos
```
php artisan migrate
```
#### 7) Acceder a traves de un navegador web

``
http://midominio.com
``
