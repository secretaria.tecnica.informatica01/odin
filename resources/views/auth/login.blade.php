@extends('layouts/fullLayoutMaster')

@section('title', 'Login')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/authentication.css')) }}">
@endsection

@section('content')
  <div class="auth-wrapper auth-basic px-2">
    <div class="auth-inner my-2">
      <!-- Login basic -->
      <div class="card mb-0">
        <div class="card-body">
          <a href="#" class="brand-logo  align-items-center">
            <img width="50" src="https://upload.wikimedia.org/wikipedia/commons/b/bd/Coat_of_arms_of_Benito_Juarez%2C_Quintana_Roo.svg" alt="">
            <h2 class="brand-text text-primary ms-1 mb-0">Secretaria Tecnica</h2>
          </a>

          <p class="card-text mb-2">Inicia sesión en tu cuenta y comienza la aventura.</p>

          @if (session('status'))
            <div class="alert alert-success mb-1 rounded-0" role="alert">
              <div class="alert-body">
                {{ session('status') }}
              </div>
            </div>
          @endif

          <form class="auth-login-form mt-2" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="mb-1">
              <label for="login-username" class="form-label">Usuario</label>
              <input autocomplete="gato" type="text" class="form-control @error('email') is-invalid @enderror" id="login-username" name="username"
             aria-describedby="login-username" tabindex="1" autofocus
                value="{{ old('username') }}" />
              @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>

            <div class="mb-1">
              <div class="d-flex justify-content-between">
                <label class="form-label" for="login-password">Contraseña</label>
                @if (Route::has('password.request'))
                  <a href="{{ route('password.request') }}">
                    <small>¿Olvidaste tu contraseña?</small>
                  </a>
                @endif
              </div>
              <div class="input-group input-group-merge form-password-toggle">
                <input type="password" class="form-control form-control-merge" id="login-password" name="password"
                  tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                  aria-describedby="login-password" />
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
            <button type="submit" class="btn btn-primary w-100" tabindex="4">Entrar</button>
          </form>
        </div>
      </div>
      <!-- /Login basic -->
    </div>
  </div>
@endsection
