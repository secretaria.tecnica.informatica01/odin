@extends('layouts/contentLayoutMaster')

@section('title', 'Home')

@section('content')
<div class="row">
  <div class="col-xl-2 col-md-4 col-sm-6">
    @livewire('sam::widgets.colaboradores')
  </div>
  <div class="col-xl-2 col-md-4 col-sm-6">
    @livewire('sam::widgets.ejes')
  </div>
  <div class="col-xl-2 col-md-4 col-sm-6">
    @livewire('sam::widgets.programas')
  </div>
  <div class="col-xl-2 col-md-4 col-sm-6">
    @livewire('sam::widgets.estrategias')
  </div>
  <div class="col-xl-2 col-md-4 col-sm-6">
    @livewire('sam::widgets.areas')
  </div>
  <div class="col-xl-2 col-md-4 col-sm-6">
    @livewire('sam::widgets.actividades')
  </div>
</div>

@endsection