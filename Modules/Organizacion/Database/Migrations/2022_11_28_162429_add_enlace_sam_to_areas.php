<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("areas", function (Blueprint $table) {
            $table
                ->unsignedBigInteger("enlace_sam_id")
                ->nullable()
                ->after("titular_id");
            $table
                ->foreign("enlace_sam_id")
                ->references("id")
                ->on("colaboradores")
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("areas", function (Blueprint $table) {
            $table->dropColumn("enlace_sam_id");
        });
    }
};
