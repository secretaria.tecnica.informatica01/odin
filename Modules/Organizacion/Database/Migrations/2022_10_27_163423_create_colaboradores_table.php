<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("colaboradores", function (Blueprint $table) {
            $table->id();
            $table->foreignId('area_id')->nullable()->constrained('areas')->nullOnDelete();
            $table->string("nombres");
            $table->string("apellido_p");
            $table->string("apellido_m");
            $table->timestamps();
        });

        Schema::table("areas", function (Blueprint $table) {
            $table
                ->foreign("titular_id")
                ->references("id")
                ->on("colaboradores")
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("colaboradores");
        Schema::table("areas", function (Blueprint $table) {
            $table->dropForeign(["titular_id"]);
        });
    }
};
