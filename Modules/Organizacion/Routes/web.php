<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(OrganizacionController::class)
    ->prefix("organizacion")
    ->name("organizacion.")
    ->middleware(["auth"])
    ->group(function () {
        Route::get("/areas", "areas")->name("areas");
        Route::get("/colaboradores", "colaboradores")->name("colaboradores");
        Route::get("/organigrama", "organigrama")->name("organigrama");
    });
