<?php

namespace Modules\Organizacion\Http\Livewire\Colaboradores;

use Livewire\Component;
use Modules\Organizacion\Entities\Colaborador;

class Delete extends Component
{
    public function render()
    {
        return view("organizacion::livewire.colaboradores.delete");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Colaborador::find($model_id)->delete();
        $this->emit("colaboradorDeleted");
    }
}
