<?php

namespace Modules\Organizacion\Http\Livewire\Colaboradores;

use Livewire\Component;
use Modules\Organizacion\Entities\Area;
use Modules\Organizacion\Entities\Colaborador;

class Edit extends Create
{
    public $nombres, $apellido_p, $apellido_m, $area_id, $model_id;

    protected $listeners = [
        "editColaborador" => "edit",
    ];

    public function edit(Colaborador $colaborador)
    {
        $areas = Area::all(["id", "nombre as text"]);
        $this->model_id = $colaborador->id;
        $this->nombres = $colaborador->nombres;
        $this->apellido_p = $colaborador->apellido_p;
        $this->apellido_m = $colaborador->apellido_m;
        $this->area_id = $colaborador->area_id;
        $this->emit("setArea", $areas, $colaborador->area_id);
    }

    public function update()
    {
        $this->validate();

        Colaborador::find($this->model_id)->update([
            "nombres" => $this->nombres,
            "apellido_p" => $this->apellido_p,
            "apellido_m" => $this->apellido_m,
            "area_id" => $this->area_id,
        ]);

        $this->emit("colaboradorUpdated");
    }

    public function render()
    {
        return view("organizacion::livewire.colaboradores.edit");
    }
}
