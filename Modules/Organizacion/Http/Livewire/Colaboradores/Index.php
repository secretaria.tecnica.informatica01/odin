<?php

namespace Modules\Organizacion\Http\Livewire\Colaboradores;

use Livewire\Component;
use Modules\Organizacion\Entities\Colaborador;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "colaboradorStored" => '$refresh',
        "colaboradorUpdated" => '$refresh',
        "colaboradorDeleted" => '$refresh',
    ];

    private function getAreas()
    {
        //filtrado por query
        return Colaborador::with("user", "area")->where(function ($q) {
            $q->whereHas("user", function ($q) {
                $q->where(
                    "username",
                    "like",
                    "%" . $this->search . "%"
                )->orWhere("name", "like", "%" . $this->search . "%");
            })
                ->orWhere("nombres", "like", "%" . $this->search . "%")
                ->orWhere("apellido_p", "like", "%" . $this->search . "%")
                ->orWhere("apellido_m", "like", "%" . $this->search . "%");
        });
    }

    public function render()
    {
        $colaboradores = $this->getAreas()->paginate(10);
        return view(
            "organizacion::livewire.colaboradores.index",
            compact("colaboradores")
        );
    }
}
