<?php

namespace Modules\Organizacion\Http\Livewire\Colaboradores;

use Livewire\Component;
use Modules\Organizacion\Entities\Area;
use Modules\Organizacion\Entities\Colaborador;

class Create extends Component
{
    public $nombres, $apellido_p, $apellido_m, $area_id, $model_id;

    protected $listeners = [
        "createColaborador" => "create",
    ];

    protected function rules()
    {
        return [
            "nombres" => ["required"],
            "apellido_p" => ["required"],
            "apellido_m" => ["required"],
            "area_id" => "exists:areas,id|nullable",
        ];
    }

    public function create()
    {
        $areas = Area::all(["id", "nombre as text"]);
        $this->emit("setArea", $areas, null);
    }

    public function store()
    {
        $this->validate();

        Colaborador::create([
            "nombres" => $this->nombres,
            "apellido_p" => $this->apellido_p,
            "apellido_m" => $this->apellido_m,
            "area_id" => $this->area_id,
        ]);

        $this->create();

        $this->resetInputFields();

        $this->emit("colaboradorStored");
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        $this->nombres = "";
        $this->apellido_p = "";
        $this->apellido_m= "";
        $this->area_id = null;
    }
    public function render()
    {
        return view("organizacion::livewire.colaboradores.create");
    }
}
