<?php

namespace Modules\Organizacion\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Colaborador extends Component
{
    public function render()
    {
        $nombre = "";
        $user = Auth::user();
        $roles = [];
        if (isset($user)) {
            $nombre = $user->name;
            $roles = $user->getRoleNames();
        }
        if (isset($user->colaborador)) {
            $nombre = $user->colaborador->full_name;
        }
        return view(
            "organizacion::livewire.widgets.colaborador",
            compact("roles", "nombre")
        );
    }
}
