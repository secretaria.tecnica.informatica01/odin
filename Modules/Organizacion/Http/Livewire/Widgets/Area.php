<?php

namespace Modules\Organizacion\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class Area extends Component
{
    public function render()
    {
        $user = Auth::user();
        $dato = "";
        if (!is_null($user->colaborador) && !is_null($user->colaborador->area)) {
            $dato = $user->colaborador->area->nombre;
        }
        return view("organizacion::livewire.widgets.area", compact("dato"));
    }
}
