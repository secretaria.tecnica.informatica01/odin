<?php

namespace Modules\Organizacion\Http\Livewire\Areas;
use Livewire\Component;
use Modules\Plan\Entities\Eje;
use Illuminate\Validation\Rule;
use Modules\Plan\Entities\Programa;
use Modules\Plan\Entities\Estrategia;
use Modules\Organizacion\Entities\Area;
use Modules\Organizacion\Entities\Colaborador;

class Create extends Component
{
    public $nombre, $area_id, $titular_id, $model_id;
    public $enlace_sam_id,
        $ejes_sam = [],
        $programas_sam = [],
        $estrategias_sam = [],
        $revisores_sam = [];
    public $ejes = [],
        $programas = [],
        $estrategias = [],
        $areas = [],
        $colaboradores = [];

    protected $listeners = [
        "createArea" => "create",
    ];

    protected function rules()
    {
        $rules = [
            "nombre" => [
                "required",
                Rule::unique("areas")->ignore($this->model_id),
            ],
            "titular_id" => "exists:colaboradores,id|nullable",
            "enlace_sam_id" => "exists:colaboradores,id|nullable",
        ];

        if (isset($this->model_id)) {
            $rules["area_id"] = [
                "exists:areas,id",
                "nullable",
                Rule::notIn([$this->model_id]),
            ];
        } else {
            $rules["area_id"] = "exists:areas,id|nullable";
        }

        return $rules;
    }

    public function render()
    {
        return view("organizacion::livewire.areas.create");
    }

    public function mount()
    {
        $this->ejes = Eje::all(["id", "nombre"])->pluck("nombre", "id");
        $this->programas = Programa::all(["id", "nombre"])->pluck(
            "nombre",
            "id"
        );
        $this->estrategias = Estrategia::all(["id", "nombre"])->pluck(
            "nombre",
            "id"
        );

        $this->colaboradores = Colaborador::all()->pluck("full_name", "id");
        $this->areas = Area::all(["id", "nombre"])->pluck("nombre", "id");
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        $this->nombre = "";
        $this->area_id = null;
        $this->titular_id = null;
        $this->enlace_sam_id = null;

        $this->ejes_sam = [];
        $this->programas_sam = [];
        $this->estrategias_sam = [];
        $this->revisores_sam = [];

        
        $this->emit("setTitular", $this->titular_id);
        $this->emit("setEnlaceSam", $this->enlace_sam_id);
        $this->emit("setArea", $this->area_id);

        $this->emit("setEjesSam", $this->ejes_sam);
        $this->emit("setProgramasSam", $this->programas_sam);
        $this->emit("setEstrategiasSam", $this->estrategias_sam);
        $this->emit("setRevisoresSam", $this->revisores_sam);
    }

    public function store()
    {
        $this->validate();

        $area = Area::create([
            "nombre" => $this->nombre,
            "area_id" => $this->area_id,
            "titular_id" => $this->titular_id,
            "enlace_sam_id" => $this->enlace_sam_id,
        ]);

        $area->ejes_sam()->sync($this->ejes_sam);
        $area->programas_sam()->sync($this->programas_sam);
        $area->estrategias_sam()->sync($this->estrategias_sam);
        $area->revisores_sam()->sync($this->revisores_sam);

        $this->resetInputFields();

        $this->emit("areaStored");
    }
}
