<?php

namespace Modules\Organizacion\Http\Livewire\Areas;
use Livewire\Component;
use Modules\Plan\Entities\Eje;
use Illuminate\Validation\Rule;
use Modules\Plan\Entities\Estrategia;
use Modules\Organizacion\Entities\Area;
use Modules\Organizacion\Entities\Colaborador;

class Edit extends Create
{
    protected $listeners = [
        "editArea" => "edit",
    ];

    public function render()
    {
        return view("organizacion::livewire.areas.edit");
    }

    public function edit(Area $area)
    {
        $areas = Area::all(["id", "nombre as text"])->except($area->id);
        $ejes = Eje::all(["id", "nombre as text"]);
        $estrategias = Estrategia::all(["id", "nombre as text"]);
        $this->emit("setTitular", $area->titular_id);
        $this->emit("setEnlaceSam", $area->enlace_sam_id);
        $this->emit("setArea", $area->area_id);

        $this->ejes_sam = $area->ejes_sam->pluck("id");
        $this->programas_sam = $area->programas_sam->pluck("id");
        $this->estrategias_sam = $area->estrategias_sam->pluck("id");
        $this->revisores_sam = $area->revisores_sam->pluck("id");
        
        $this->emit("setEjesSam", $this->ejes_sam);
        $this->emit("setProgramasSam", $this->programas_sam);
        $this->emit("setEstrategiasSam", $this->estrategias_sam);
        $this->emit("setRevisoresSam", $this->revisores_sam);

        $this->model_id = $area->id;
        $this->nombre = $area->nombre;
        $this->area_id = $area->area_id;
        $this->titular_id = $area->titular_id;
        $this->enlace_sam_id = $area->enlace_sam_id;
    }

    public function update()
    {
        $this->validate();

        $area = Area::find($this->model_id);
        $area->update([
            "nombre" => $this->nombre,
            "area_id" => $this->area_id,
            "titular_id" => $this->titular_id,
            "enlace_sam_id" => $this->enlace_sam_id,
        ]);

        $area->ejes_sam()->sync($this->ejes_sam);
        $area->programas_sam()->sync($this->programas_sam);
        $area->estrategias_sam()->sync($this->estrategias_sam);
        $area->revisores_sam()->sync($this->revisores_sam);
        $this->emit("areaUpdated");
    }
}
