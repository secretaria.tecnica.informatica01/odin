<?php

namespace Modules\Organizacion\Http\Livewire\Areas;

use Livewire\Component;
use Modules\Organizacion\Entities\Area;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "areaStored" => '$refresh',
        "areaUpdated" => '$refresh',
        "areaDeleted" => '$refresh',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    private function getAreas()
    {
        //filtrado por query
        return Area::with("titular", "area", "areas")
            ->withCount("actividades")
            ->where(function ($q) {
                $q->whereHas("titular", function ($q) {
                    $q->where("nombres", "like", "%" . $this->search . "%")
                        ->orWhere(
                            "apellido_p",
                            "like",
                            "%" . $this->search . "%"
                        )
                        ->orWhere(
                            "apellido_m",
                            "like",
                            "%" . $this->search . "%"
                        );
                })->orWhere("nombre", "like", "%" . $this->search . "%");
            });
    }

    public function render()
    {
        $areas = $this->getAreas()->paginate(10);
        return view("organizacion::livewire.areas.index", compact("areas"));
    }
}
