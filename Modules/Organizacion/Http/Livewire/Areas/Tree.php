<?php

namespace Modules\Organizacion\Http\Livewire\Areas;

use Livewire\Component;

class Tree extends Component
{
    public function render()
    {
        return view('organizacion::livewire.areas.tree');
    }
}
