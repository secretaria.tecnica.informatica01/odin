<?php

namespace Modules\Organizacion\Http\Livewire\Areas;

use Livewire\Component;
use Modules\Organizacion\Entities\Area;

class Organigrama extends Component
{
    public $model_id, $area;

    public function render()
    {
        return view("organizacion::livewire.areas.organigrama");
    }

    public function mount(Area $area)
    {
        $area->load('children');
        $this->area = $area;

    }
}
