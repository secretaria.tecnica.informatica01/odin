<?php

namespace Modules\Organizacion\Http\Livewire\Areas;

use Livewire\Component;
use Modules\Organizacion\Entities\Area;


class Delete extends Component
{
    public function render()
    {
        return view('organizacion::livewire.areas.delete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Area::find($model_id)->delete();
        $this->emit("areaDeleted");
    }
}