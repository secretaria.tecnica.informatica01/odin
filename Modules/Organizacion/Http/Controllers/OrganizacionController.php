<?php

namespace Modules\Organizacion\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Organizacion\Entities\Area;

class OrganizacionController extends Controller
{
    public function __construct()
    {
        $this->middleware("permission:org areas listar")->only(
            "areas"
        );

        $this->middleware("permission:org colaboradores listar")->only(
            "colaboradores"
        );
    }
    public function areas()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "organizacion/areas", "name" => "Áreas"],
            ["name" => "Listado"],
        ];
        return view("organizacion::areas", compact("breadcrumbs"));
    }
    public function colaboradores()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "organizacion/colaboradores", "name" => "Colaboradores"],
            ["name" => "Listado"],
        ];
        return view("organizacion::colaboradores", compact("breadcrumbs"));
    }
    public function organigrama()
    {
        $tops = Area::with("titular")
            ->whereNull("area_id")
            ->get();
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "organizacion/organigrama", "name" => "Organigrama"],
            ["name" => "Vista previa"],
        ];
        return view(
            "organizacion::organigrama",
            compact("breadcrumbs", "tops")
        );
    }
}
