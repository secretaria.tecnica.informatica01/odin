@extends('layouts.contentLayoutMaster')
@section('title', 'Organigrama')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/orgchart/orgchart.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h3>Vista previa de organigrama</h3>
        <p class="mb-2">
            Un organigrama muestra la estructura interna de una organización o empresa. Los empleados y sus cargos se
            representan con rectángulos y otras figuras.
        </p>
    </div>
</div>
@foreach($tops as $top)
@livewire('organizacion::areas.organigrama',['area'=>$top])
@endforeach
@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('js/scripts/orgchart/orgchart.js')) }}"></script>

@endsection