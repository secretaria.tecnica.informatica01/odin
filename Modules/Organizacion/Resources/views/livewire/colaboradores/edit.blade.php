<div>
    <div wire:ignore.self class="modal fade" id="editColaboradorModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Edición de colaborador</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-nombres">Nombres</label>
                            <input wire:model="nombres" id="old-nombres" type="text"
                                class="form-control @error('nombres') is-invalid @enderror"
                                placeholder="Ingrese nombre para el área" tabindex="1" />
                            @error('nombres') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-apellido_p">Apellido paterno</label>
                            <input wire:model="apellido_p" id="old-apellido_p" type="text"
                                class="form-control @error('apellido_p') is-invalid @enderror"
                                placeholder="Ingrese nombre para el área" tabindex="1" />
                            @error('apellido_p') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-apellido_m">Apellido materno</label>
                            <input wire:model="apellido_m" id="old-apellido_m" type="text"
                                class="form-control @error('apellido_m') is-invalid @enderror"
                                placeholder="Ingrese nombre para el área" tabindex="1" />
                            @error('apellido_m') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="old-area">Área</label>
                                <select class="form-select" id="old-area">
                                    <option></option>
                                </select>
                                @error('area_id') <span class="invalid-feedback">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="update()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {

        $('#old-area').on('change', function (e) {
            var data = $('#old-area').select2("val");
            @this.set('area_id', data);
        });
        @this.on('setArea', (areas, area) => {
            $('#old-area').empty().select2({
                data: areas,
                placeholder: "Selecione el área",
                allowClear: true,
                dropdownParent: $('#editColaboradorModal')
            }).val(area).trigger('change.select2');

        });

        @this.on('colaboradorUpdated', () => {
            $('#editColaboradorModal').modal('hide');
            toastr['info']('', 'Colaborador actualizado', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });

    });
</script>
@endpush