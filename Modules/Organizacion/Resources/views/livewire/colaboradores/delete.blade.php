<div>
    @push('scripts')
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            window.livewire.on('triggerDelete', model_id => {
                Swal.fire({
                    title: '¿Esta seguro?',
                    text: '¡El colaborador será eliminada!',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        // calling destroy method to delete
                        @this.call('destroy', model_id);
                        // success response
                        Swal.fire({ title: 'Colaborador eliminado correctamente!', icon: 'success' });
                    }
                });
            });
        })
    </script>
    @endpush
</div>