<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$colaboradores->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre Completo</th>
                        <th>Usuario</th>
                        <th>Área</th>
                        <th>Fecha de creación</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($colaboradores as $colaborador)
                    <tr>
                        <td>
                            {{$colaborador->full_name}}
                        </td>
                        <td>
                            @if(isset($colaborador->user))
                            <span class="fw-bold">{{$colaborador->user->username}}</span>
                            @else
                            Sin usuario
                            @endif
                        </td>
                        <td>
                            @if(isset($colaborador->area))
                            <span class="fw-bold">{{$colaborador->area->nombre}}</span>
                            @endif
                        </td>
                        <td>
                            {{$colaborador->created_at}}
                        </td>
                        <td>
                            <button data-bs-target="#editColaboradorModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('editColaborador','{{ $colaborador->id }}')">
                                Editar</button>
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerDelete','{{ $colaborador->id }}')">
                                Eliminar
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>