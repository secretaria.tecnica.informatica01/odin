<div class="user-nav d-sm-flex d-none">
    <span class="user-name fw-bolder">
        {{$nombre}}
    </span>
    <div class="d-flex">
        @foreach($roles as $role)
        <span class="badge rounded-pill badge-light-primary ms-1">
            {{$role}}
        </span>
        @endforeach
    </div>

</div>