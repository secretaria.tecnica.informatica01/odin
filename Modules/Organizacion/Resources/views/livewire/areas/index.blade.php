<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda por titular o área">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$areas->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Área</th>
                        <th>Titular</th>
                        <th>Área superior</th>
                        <th>Subáreas</th>
                        <th>Fecha de creación</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($areas as $index => $area)
                    <tr>
                        <td>
                            {{$area->nombre}}
                        </td>
                        <td>
                            @if(isset($area->titular))
                            <span class="fw-bold">{{$area->titular->fullName}}</span>
                            @endif
                        </td>
                        <td>
                            @if(isset($area->area))
                            <span class="fw-bold">{{$area->area->nombre}}</span>
                            @endif
                        </td>
                        <td>
                            @foreach ($area->areas as $subarea)
                            <span class="badge rounded-pill badge-light-primary">
                                {{$subarea->nombre}}
                            </span>
                            @endforeach
                        </td>
                        <td>
                            {{$area->created_at}}
                        </td>
                        <td>
                            <button data-bs-target="#editAreaModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('editArea','{{ $area->id }}')">
                                Editar</button>
                            @if(!(count($area->areas) + $area->actividades_count))
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerDelete','{{ $area->id }}')">
                                Eliminar
                            </button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>