<div>
    <div wire:ignore.self class="modal fade" id="addAreaModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Edición de área</h2>
                    </div>

                    <div class="d-flex align-items-start justify-content-between">
                        <div wire:ignore class="nav flex-column nav-pills me-3" role="tablist"
                            aria-orientation="vertical">
                            <button class="nav-link active" data-bs-toggle="pill" data-bs-target="#v-pills-create-1"
                                type="button" role="tab" aria-selected="true">General</button>
                            <button class="nav-link" data-bs-toggle="pill" data-bs-target="#v-pills-create-2"
                                type="button" role="tab" aria-selected="false">SAM</button>
                        </div>
                        <div class="tab-content w-100">
                            <div wire:ignore.self class="tab-pane fade show active" id="v-pills-create-1"
                                role="tabpanel" tabindex="0">
                                <!-- Add role form -->
                                <div class="row gy-1">
                                    <div class="col-12">
                                        <label class="form-label fw-bolder" for="new-nombre">Nombre</label>
                                        <input wire:model="nombre" id="new-nombre" type="text" name="nombre"
                                            class="form-control @error('nombre') is-invalid @enderror"
                                            placeholder="Ingrese nombre para el área" tabindex="1" />
                                        @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="new-area">Área superior</label>
                                            <select class="form-select" id="new-area">
                                                <option></option>
                                                @foreach($areas as $id => $area)
                                                <option value="{{$id}}">{{$area}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('area_id') <span class="invalid-feedback d-block">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="new-titular">Titular</label>
                                            <select class="form-select" id="new-titular">
                                                <option></option>
                                                @foreach($colaboradores as $id => $cola)
                                                <option value="{{$id}}">{{$cola}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        @error('titular_id') <span class="invalid-feedback d-block">{{ $message
                                            }}</span> @enderror
                                    </div>
                                </div>
                                <!--/ Add role form -->
                            </div>
                            <div style="max-width: 80%;" wire:ignore.self class="tab-pane fade" id="v-pills-create-2" role="tabpanel"
                                tabindex="0">
                                <div class="row gy-1">

                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="new-enlace-sam">Enlace</label>
                                            <select class="form-select" id="new-enlace-sam">
                                                <option></option>
                                                @foreach($colaboradores as $id => $cola)
                                                <option value="{{$id}}">{{$cola}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('enlace_sam_id') <span class="invalid-feedback d-block">{{ $message
                                            }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder"
                                                for="new-revisores-sam">Revisores</label>
                                            <select class="form-select" id="new-revisores-sam" multiple>
                                                @foreach($colaboradores as $id => $cola)
                                                <option data-ids="{{$id}}" value="{{$id}}">{{$cola}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('revisores_sam') <span class="invalid-feedback d-block">{{ $message
                                            }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="new-ejes-sam">Ejes</label>
                                            <select class="form-select" id="new-ejes-sam" multiple>
                                                @foreach($ejes as $id => $eje)
                                                <option value="{{$id}}">{{$eje}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('ejes_sam') <span class="invalid-feedback d-block">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder"
                                                for="new-programas-sam">Programas</label>
                                            <select class="form-select" id="new-programas-sam" multiple>
                                                @foreach($programas as $id => $programa)
                                                <option value="{{$id}}">{{$programa}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('programas_sam') <span class="invalid-feedback d-block">{{ $message
                                            }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder"
                                                for="new-estrategias-sam">Estrategias</label>
                                            <select class="form-select" id="new-estrategias-sam" multiple>
                                                @foreach($estrategias as $id => $est)
                                                <option value="{{$id}}">{{$est}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('estrategias_sam') <span class="invalid-feedback d-block">{{
                                            $message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        let revisores = [];

        //bloque de titular
        $('#new-titular').select2({
            placeholder: "Selecione el titular del área",
            allowClear: true,
            dropdownParent: $('#addAreaModal')
        });

        $('#new-titular').on('change', function (e) {
            var data = $('#new-titular').select2("val");
            @this.set('titular_id', data, true);
        });

        @this.on('setTitular', (titular) => {
            $('#new-titular').val(titular).trigger('change.select2');
        });

        //bloque de revisores
        $('#new-revisores-sam').select2({
            placeholder: "Selecione los revisores",
            dropdownParent: $('#addAreaModal'),
        });

        @this.on('setRevisoresSam', (revisores_sam) => {
            $('#new-revisores-sam').val(revisores_sam).trigger('change.select2');
        });

        $('#new-revisores-sam').on('select2:select', function (e) {
            e.params.data.id;
            revisores.push(e.params.data.id);
            console.log(revisores);
            @this.set('revisores_sam', revisores, true);
        });

        $('#new-revisores-sam').on('select2:unselect', function (e) {
            e.params.data.id;
            revisores.splice(revisores.indexOf(e.params.data.id), 1);
            console.log(revisores);
            @this.set('revisores_sam', revisores, true);
        });

        //bloque de enlace sam
        $('#new-enlace-sam').select2({
            placeholder: "Selecione un enlace",
            dropdownParent: $('#addAreaModal')
        });

        $('#new-enlace-sam').on('change', function (e) {
            var data = $('#new-enlace-sam').select2("val");
            @this.set('enlace_sam_id', data, true);
        });

        @this.on('setEnlaceSam', (enlace_sam) => {
            $('#new-enlace-sam').val(enlace_sam).trigger('change.select2');
        });


        //bloque area superior
        $('#new-area').on('change', function (e) {
            var data = $('#new-area').select2("val");
            @this.set('area_id', data, true);
        });

        $('#new-area').select2({
            placeholder: "Selecione el area superior",
            allowClear: true,
            dropdownParent: $('#addAreaModal')
        });

        @this.on('setArea', (area) => {
            $('#new-area').val(area).trigger('change.select2');
        });

        //bloque ejes sam
        $('#new-ejes-sam').select2({
            placeholder: "Selecione los ejes",
            allowClear: true,
            dropdownParent: $('#addAreaModal')
        });

        $('#new-ejes-sam').on('change', function (e) {
            var data = $('#new-ejes-sam').select2("val");
            @this.set('ejes_sam', data, true);
        });
        @this.on('setEjesSam', (ejes) => {
            $('#new-ejes-sam').val(ejes).trigger('change.select2');
        });

        //bloque de programas sam
        $('#new-programas-sam').select2({
            placeholder: "Selecione los programas",
            allowClear: true,
            dropdownParent: $('#addAreaModal')
        });

        @this.on('setProgramasSam', (progsam) => {
            $('#new-programas-sam').val(progsam).trigger('change.select2');
        });

        $('#new-programas-sam').on('change', function (e) {
            var data = $('#new-programas-sam').select2("val");
            @this.set('programas_sam', data, true);
        });

        //bloque de estrategias
        $('#new-estrategias-sam').select2({
            placeholder: "Selecione los ejes",
            allowClear: true,
            dropdownParent: $('#addAreaModal')
        });

        $('#new-estrategias-sam').on('change', function (e) {
            var data = $('#new-estrategias-sam').select2("val");
            @this.set('estrategias_sam', data, true);
        });

        @this.on('setEstrategiasSam', (estrategiassam) => {
            $('#new-estrategias-sam').val(estrategiassam).trigger('change.select2');
        });

        @this.on('areaStored', () => {
            toastr['success']('', 'Área guardada', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });

    });
</script>
@endpush