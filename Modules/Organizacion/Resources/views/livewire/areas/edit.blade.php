<div>
    <div wire:ignore.self class="modal fade" id="editAreaModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Edición de área</h2>
                    </div>

                    <div class="d-flex align-items-start justify-content-between">
                        <div wire:ignore class="nav flex-column nav-pills me-3" role="tablist"
                            aria-orientation="vertical">
                            <button class="nav-link active" data-bs-toggle="pill" data-bs-target="#v-pills-edit-1"
                                type="button" role="tab" aria-selected="true">General</button>
                            <button class="nav-link" data-bs-toggle="pill" data-bs-target="#v-pills-edit-2"
                                type="button" role="tab" aria-selected="false">SAM</button>
                        </div>
                        <div class="tab-content w-100">
                            <div wire:ignore.self class="tab-pane fade show active" id="v-pills-edit-1" role="tabpanel"
                                tabindex="0">
                                <!-- Add role form -->
                                <div class="row gy-1">
                                    <div class="col-12">
                                        <label class="form-label fw-bolder" for="old-nombre">Nombre</label>
                                        <input wire:model="nombre" id="old-nombre" type="text" name="nombre"
                                            class="form-control @error('nombre') is-invalid @enderror"
                                            placeholder="Ingrese nombre para el área" tabindex="1" />
                                        @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="old-area">Área superior</label>
                                            <select class="form-select" id="old-area">
                                                @foreach($areas as $id => $area)
                                                <option value="{{$id}}">{{$area}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('area_id') <span class="invalid-feedback d-block">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="old-titular">Titular</label>
                                            <select class="form-select" id="old-titular">
                                                @foreach($colaboradores as $id => $cola)
                                                <option value="{{$id}}">{{$cola}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        @error('titular_id') <span class="invalid-feedback d-block">{{ $message
                                            }}</span> @enderror
                                    </div>
                                </div>
                                <!--/ Add role form -->
                            </div>
                            <div style="max-width: 80%;" wire:ignore.self class="tab-pane fade" id="v-pills-edit-2" role="tabpanel"
                                tabindex="0">
                                <div class="row gy-1">

                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="old-enlace-sam">Enlace</label>
                                            <select class="form-select" id="old-enlace-sam">
                                                @foreach($colaboradores as $id => $cola)
                                                <option value="{{$id}}">{{$cola}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('enlace_sam_id') <span class="invalid-feedback d-block">{{ $message
                                            }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder"
                                                for="old-revisores-sam">Revisores</label>
                                            <select class="form-select" id="old-revisores-sam" multiple>
                                                @foreach($colaboradores as $id => $cola)
                                                <option data-ids="{{$id}}" value="{{$id}}">{{$cola}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('revisores_sam') <span class="invalid-feedback d-block">{{ $message
                                            }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder" for="old-ejes-sam">Ejes</label>
                                            <select class="form-select" id="old-ejes-sam" multiple>
                                                @foreach($ejes as $id => $eje)
                                                <option value="{{$id}}">{{$eje}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('ejes_sam') <span class="invalid-feedback d-block">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder"
                                                for="old-programas-sam">Programas</label>
                                            <select class="form-select" id="old-programas-sam" multiple>
                                                @foreach($programas as $id => $programa)
                                                <option value="{{$id}}">{{$programa}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('programas_sam') <span class="invalid-feedback d-block">{{ $message
                                            }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-12">
                                        <div wire:ignore>
                                            <label class="form-label fw-bolder"
                                                for="old-estrategias-sam">Estrategias</label>
                                            <select class="form-select" id="old-estrategias-sam" multiple>
                                                @foreach($estrategias as $id => $est)
                                                <option value="{{$id}}">{{$est}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('estrategias_sam') <span class="invalid-feedback d-block">{{
                                            $message}}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="update()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {

        let revisores = [];

        //bloque de titular
        $('#old-titular').select2({
            placeholder: "Selecione el titular del área",
            allowClear: true,
            dropdownParent: $('#editAreaModal')
        });

        $('#old-titular').on('change', function (e) {
            var data = $('#old-titular').select2("val");
            @this.set('titular_id', data, true);
        });

        @this.on('setTitular', (titular) => {
            $('#old-titular').val(titular).trigger('change.select2');
        });

        //bloque de revisores
        $('#old-revisores-sam').select2({
            placeholder: "Selecione los revisores",
            dropdownParent: $('#editAreaModal'),
        });

        @this.on('setRevisoresSam', (revisores_sam) => {
            $('#old-revisores-sam').val(revisores_sam).trigger('change.select2');
        });

        $('#old-revisores-sam').on('select2:select', function (e) {
            e.params.data.id;
            revisores.push(e.params.data.id);
            console.log(revisores);
            @this.set('revisores_sam', revisores, true);
        });

        $('#old-revisores-sam').on('select2:unselect', function (e) {
            e.params.data.id;
            revisores.splice(revisores.indexOf(e.params.data.id), 1);
            console.log(revisores);
            @this.set('revisores_sam', revisores, true);
        });

        //bloque de enlace sam
        $('#old-enlace-sam').select2({
            placeholder: "Selecione un enlace",
            dropdownParent: $('#editAreaModal')
        });

        $('#old-enlace-sam').on('change', function (e) {
            var data = $('#old-enlace-sam').select2("val");
            @this.set('enlace_sam_id', data, true);
        });

        @this.on('setEnlaceSam', (enlace_sam) => {
            $('#old-enlace-sam').val(enlace_sam).trigger('change.select2');
        });


        //bloque area superior
        $('#old-area').on('change', function (e) {
            var data = $('#old-area').select2("val");
            @this.set('area_id', data, true);
        });

        $('#old-area').select2({
            placeholder: "Selecione el area superior",
            allowClear: true,
            dropdownParent: $('#editAreaModal')
        });

        @this.on('setArea', (area) => {
            $('#old-area').val(area).trigger('change.select2');
        });

        //bloque ejes sam
        $('#old-ejes-sam').select2({
            placeholder: "Selecione los ejes",
            allowClear: true,
            dropdownParent: $('#editAreaModal')
        });

        $('#old-ejes-sam').on('change', function (e) {
            var data = $('#old-ejes-sam').select2("val");
            @this.set('ejes_sam', data, true);
        });
        @this.on('setEjesSam', (ejes) => {
            $('#old-ejes-sam').val(ejes).trigger('change.select2');
        });

        //bloque de programas sam
        $('#old-programas-sam').select2({
            placeholder: "Selecione los programas",
            allowClear: true,
            dropdownParent: $('#editAreaModal')
        });

        @this.on('setProgramasSam', (progsam) => {
            $('#old-programas-sam').val(progsam).trigger('change.select2');
        });

        $('#old-programas-sam').on('change', function (e) {
            var data = $('#old-programas-sam').select2("val");
            @this.set('programas_sam', data, true);
        });

        //bloque de estrategias
        $('#old-estrategias-sam').select2({
            placeholder: "Selecione los ejes",
            allowClear: true,
            dropdownParent: $('#editAreaModal')
        });

        $('#old-estrategias-sam').on('change', function (e) {
            var data = $('#old-estrategias-sam').select2("val");
            @this.set('estrategias_sam', data, true);
        });

        @this.on('setEstrategiasSam', (estrategiassam) => {
            $('#old-estrategias-sam').val(estrategiassam).trigger('change.select2');
        });

        //actualizacion de mensaje
        @this.on('areaUpdated', () => {
            $('#editAreaModal').modal('hide');
            toastr['info']('', 'Área actualizada', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });

    });
</script>
@endpush