<div class="page-organigrama">
    <div class="card">
        <div class="card-body">
            <div class="orgchart-container">
                <div id="chart-container-{{$area->id}}"></div>
            </div>
        </div>
    </div>

</div>
@push('styles')
<style>
    .orgchart-container{
        position: relative;
        overflow: auto;
        text-align: center;
    }
    .orgchart-container > div > div{
        min-height: 420px;
    }
</style>
@endpush
@push('scripts')

<script type="text/javascript">
    document.addEventListener('livewire:load', function () {


        var datasource = JSON.parse('{!!$area!!}');

        var nodeTemplate = function (data) {
            return `
            <a href="{{route('organizacion.areas')}}?search=${data.nombre}">Editar</a>
        <div title="${data.nombre}" class="title">${data.nombre}</div>
        <div class="content">${data.titular && data.titular.full_name || ""}</div>
      `;
        };

        var oc = $('#chart-container-{{$area->id}}').orgchart({
            'data': datasource,
            'nodeTemplate': nodeTemplate,
            'pan': true,
            'createNode': function($node, data) {
                console.log($node);
                $node.on('doubleclick',function(){
                   alert(1); 
                });
            }
        });
    });
</script>
@endpush