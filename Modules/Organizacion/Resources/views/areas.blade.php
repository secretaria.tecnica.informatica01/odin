@extends('layouts.contentLayoutMaster')
@section('title', 'Áreas')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de áreas</h3>
    <p class="mb-2">
    Las áreas son agrupaciones de actividades, de carácter homogéneo y coordinado, que se realizan para alcanzar los objetivos organizacionales.
    </p>
  </div>
  
  <div class="col-auto">
    <button click data-bs-target="#addAreaModal" data-bs-toggle="modal" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('organizacion::areas.index')
@livewire('organizacion::areas.create')
@livewire('organizacion::areas.edit')
@livewire('organizacion::areas.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection