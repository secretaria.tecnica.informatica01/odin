<?php

namespace Modules\Organizacion\Entities;

use Modules\Plan\Entities\Eje;
use Modules\Plan\Entities\Programa;
use Modules\Sam\Entities\Actividad;
use Modules\Plan\Entities\Estrategia;
use Illuminate\Database\Eloquent\Model;
use Modules\Organizacion\Entities\Colaborador;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Area extends Model
{
    use HasFactory;

    protected $fillable = ["area_id", "nombre", "titular_id", "enlace_sam_id"];

    /**
     * Obtener las sub-areas
     */
    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function titular()
    {
        return $this->belongsTo(Colaborador::class, "titular_id");
    }

    public function children()
    {
        return $this->areas()->with(["children", "titular"]);
    }
    public function children_single()
    {
        return $this->areas()->with(["children_single"]);
    }

    public function ejes_sam()
    {
        return $this->morphedByMany(Eje::class, "pmd", "area_pmd");
    }
    public function programas_sam()
    {
        return $this->morphedByMany(Programa::class, "pmd", "area_pmd");
    }
    public function estrategias_sam()
    {
        return $this->morphedByMany(Estrategia::class, "pmd", "area_pmd");
    }

    public function revisores_sam()
    {
        return $this->morphedByMany(Colaborador::class, "pmd", "area_pmd");
    }

    public function getSubareasFlatAttribute()
    {
        return $this->flat_descendants($this);
    }

    public function getAllAreasFlatAttribute()
    {
        return collect(array_merge([$this], $this->flat_descendants($this)));
    }

    private function flat_descendants($model)
    {
        $result = [];
        foreach ($model->areas as $area) {
            $result[] = $area;
            if ($area->areas) {
                $result = array_merge($result, $this->flat_descendants($area));
            }
        }
        return $result;
    }
}
