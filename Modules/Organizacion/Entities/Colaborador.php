<?php

namespace Modules\Organizacion\Entities;

use App\Models\User;
use App\Models\Revision;
use Modules\Sam\Entities\Historico;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Colaborador extends Model
{
    use HasFactory;

    protected $table = "colaboradores";

    protected $fillable = ["area_id", "nombres", "apellido_m", "apellido_p"];

    protected $appends = ["full_name"];

    /**
     * Obtener el area al que pertenece
     */
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * Obtener todas las areas en las que puede participar segun la jerarquia
     */

    public function getAreasAttribute()
    {
        if ($this->area) {
            return $this->area->all_areas_flat;
        }
        return collect([]);
    }

    /**
     * Obtener el usuario principal que tiene
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function getFullNameAttribute()
    {
        return $this->nombres .
            " " .
            $this->apellido_p .
            " " .
            $this->apellido_m;
    }

    /**
     * Obtener todos los usuarios que tiene
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Obtener todas las revisiones que le pertenecen
     */
    public function revisiones()
    {
        return $this->hasMany(Revision::class, "colaborador_id");
    }

    /**
     * Obtener las revisiones de historicos que le pertenecen
     */
    public function revisiones_sam()
    {
        return $this->hasMany(Revision::class, "colaborador_id")->where(
            "revisable_type",
            Historico::class
        );
    }
}
