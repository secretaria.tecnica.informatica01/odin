<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Seguridad\Entities\Modulo;
use Illuminate\Contracts\Support\Renderable;

class SeguridadController extends Controller
{
    public function __construct()
    {
        $this->middleware("permission:usuarios listar")->only(
            "usuarios"
        );

        $this->middleware("permission:permisos listar")->only(
            "permisos"
        );

        $this->middleware("permission:modulos listar")->only(
            "modulos"
        );
    }

    public function usuarios()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "seguridad/usuarios", "name" => "Usuarios"],
            ["name" => "Listado"],
        ];
        return view("seguridad::usuarios", compact("breadcrumbs"));
    }

    public function permisos(Modulo $modulo)
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "seguridad/modulos", "name" => "Módulos"],
            ["name" => $modulo->nombre],
            ["name" => "Permisos"],
        ];
        return view(
            "seguridad::permisos",
            compact("breadcrumbs", "modulo")
        );
    }

    public function modulos()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "seguridad/modulos", "name" => "Módulos"],
            ["name" => "Listado"],
        ];
        return view("seguridad::modulos", compact("breadcrumbs"));
    }
}
