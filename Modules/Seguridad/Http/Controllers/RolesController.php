<?php

namespace Modules\Seguridad\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Seguridad\Entities\Modulo;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware("permission:roles listar")->only(
            "index"
        );

        $this->middleware("permission:roles editar")->only(
            "edit"
        );

        $this->middleware("permission:roles crear")->only(
            "create"
        );
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "seguridad/roles", "name" => "Roles"],
            ["name" => "Listado"],
        ];
        return view("seguridad::roles.index", compact("breadcrumbs"));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "seguridad/roles", "name" => "Roles"],
            ["name" => "Nuevo rol"],
        ];

        return view("seguridad::roles.create", compact("breadcrumbs"));
    }

    /**
     * Show the form for editing the specified resource.
     * @param Role $role
     * @return Renderable
     */
    public function edit(Role $rol)
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["link" => "seguridad/roles", "name" => "Roles"],
            ["name" => $rol->name],
            ["name" => "Edición"],
        ];
        return view("seguridad::roles.edit", compact("breadcrumbs", "rol"));
    }
}
