<?php

namespace Modules\Seguridad\Http\Livewire\Usuarios;

use Livewire\Component;
use App\Models\User;


class Delete extends Component
{
    public function render()
    {
        return view('seguridad::livewire.usuarios.delete');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        User::find($model_id)->delete();
        $this->emit("usuarioDeleted");
    }
}
