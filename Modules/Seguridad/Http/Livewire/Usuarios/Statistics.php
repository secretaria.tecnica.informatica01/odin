<?php

namespace Modules\Seguridad\Http\Livewire\Usuarios;

use Livewire\Component;
use App\Models\User;

class Statistics extends Component
{
    
    protected $listeners = [
        "usuarioStored" => '$refresh',
        "usuarioDeleted" => '$refresh',
    ];

    public function render()
    {
        $this->total = User::count();
        $this->pendientes = User::whereNull("email_verified_at")->count();
        $this->activos = User::whereNotNull("email_verified_at")
            ->whereNull("deleted_at")
            ->count();
        $this->inactivos = User::onlyTrashed()->count();
        
        return view("seguridad::livewire.usuarios.statistics");
    }
}
