<?php

namespace Modules\Seguridad\Http\Livewire\Usuarios;

use App\Models\Team;
use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Modules\Organizacion\Entities\Colaborador;

class Create extends Component //$name,
{
    public $email,
        $username,
        $password,
        $colaborador_id = null;
    public $roles = [],
        $colaboradores = [];
    public $selectedRoles = [];
    public function resetInputFields()
    {
        //$this->name = "";
        $this->email = "";
        $this->username = "";
        $this->password = "";
        $this->selectedRoles = [];
        $this->colaborador_id = null;
        $this->resetValidation();
        $this->emit("setRoles", []);
        $this->emit("setColaborador", null);
    }
    public function store()
    {
        $this->validate([
            //"name" => "required",
            "username" => "required|unique:users",
            "password" => "required",
            "email" => "unique:users",
        ]);

        //$pass = Str::random(10);
        $pass = $this->password;
        DB::beginTransaction();

        $user = User::create([
            "name" => $this->username,
            "username" => $this->username,
            "email" => $this->email,
            "password" => Hash::make($pass),
            "colaborador_id" =>
                $this->colaborador_id > 0 ? $this->colaborador_id : null,
        ]);

        $user->syncRoles($this->selectedRoles);

        $this->createTeam($user);

        DB::commit();

        $this->resetInputFields();

        $this->emit("usuarioStored");
    }
    public function render()
    {
        return view("seguridad::livewire.usuarios.create");
    }

    public function mount()
    {
        $this->roles = Role::all();
        $this->colaboradores = Colaborador::all();
    }

    /**
     * Create a personal team for the user.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    protected function createTeam(User $user)
    {
        $user->ownedTeams()->save(
            Team::forceCreate([
                "user_id" => $user->id,
                "name" => explode(" ", $user->name, 2)[0] . "'s Team",
                "personal_team" => true,
            ])
        );
    }
}
