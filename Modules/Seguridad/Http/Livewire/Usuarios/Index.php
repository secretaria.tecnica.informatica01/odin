<?php

namespace Modules\Seguridad\Http\Livewire\Usuarios;

use Livewire\Component;
use App\Models\User;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "usuarioStored" => '$refresh',
        "usuarioUpdated" => '$refresh',
        "usuarioDeleted" => '$refresh',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $usuarios = User::with("roles")
            ->where("name", "like", "%" . $this->search . "%")
            ->orWhere("email", "like", "%" . $this->search . "%")
            ->paginate(10);
        return view("seguridad::livewire.usuarios.index", compact("usuarios"));
    }
}
