<?php

namespace Modules\Seguridad\Http\Livewire\Usuarios;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Modules\Organizacion\Entities\Colaborador;

class Edit extends Component
{
    public $model_id; //$name,
    public $email,
        $username,
        $password,
        $colaborador_id = null;
    public $roles = [],
        $colaboradores = [];
    public $selectedRoles = [];

    protected $listeners = [
        "usuarioEdit" => "edit",
    ];

    public function resetInputFields()
    {
        //$this->name = "";
        $this->email = "";
        $this->username = "";
        $this->password = "";
        $this->model_id = null;
        $this->selectedRoles = [];
        $this->colaborador_id = null;
        $this->resetValidation();
        $this->emit("setRoles", []);
        $this->emit("setColaborador", null);
    }
    public function update()
    {
        $this->validate([
            //"name" => "required",
            "username" => [
                "required",
                Rule::unique("users")->ignore($this->model_id),
            ],
            "email" => [
                "required",
                Rule::unique("users")->ignore($this->model_id),
            ],
        ]);

        $user = User::find($this->model_id);
        $user->name = $this->username;
        $user->username = $this->username;
        
        if ($this->colaborador_id > 0) {
            $user->colaborador_id = $this->colaborador_id;
        } else {
            $user->colaborador_id = null;
        }

        #en caso de que el email cambie debe verificarse
        if ($user->email != $this->email) {
            $user->email = $this->email;
            $user->email_verified_at = null;
        }

        #en caso de que se mande password debe actualizarse
        if (!empty($this->password)) {
            $user->password = Hash::make($this->password);
        }

        $user->save();

        $user->syncRoles($this->selectedRoles);

        $this->resetInputFields();

        $this->emit("usuarioUpdated");
    }
    public function render()
    {
        return view("seguridad::livewire.usuarios.edit");
    }

    public function edit($model_id)
    {
        $user = User::with("roles")
            ->where("id", $model_id)
            ->first();

        $this->model_id = $model_id;
        //$this->name = $user->name;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->selectedRoles = $user->roles->pluck("name");
        $this->colaborador_id = $user->colaborador_id;
        $this->emit("setRoles", $this->selectedRoles);
        $this->emit("setColaborador", $this->colaborador_id);
    }

    public function mount()
    {
        $this->roles = Role::all();
        $this->colaboradores = Colaborador::all();
    }
}
