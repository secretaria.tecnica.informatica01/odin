<?php

namespace Modules\Seguridad\Http\Livewire\Modulos;

use Livewire\Component;
use Modules\Seguridad\Entities\Modulo;

class Index extends Component
{
    protected $listeners = [
        "moduloStored" => '$refresh',
        "moduloUpdated" => '$refresh',
        "moduloDeleted" => '$refresh',
    ];

    public function render()
    {
        $modulos = Modulo::withCount("permissions")->get();
        return view("seguridad::livewire.modulos.index", compact("modulos"));
    }
}
