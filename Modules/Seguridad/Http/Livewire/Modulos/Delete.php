<?php

namespace Modules\Seguridad\Http\Livewire\Modulos;

use Livewire\Component;
use Modules\Seguridad\Entities\Modulo;

class Delete extends Component
{
    public function render()
    {
        return view("seguridad::livewire.modulos.delete");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Modulo::find($model_id)->delete();
        $this->emit("moduloDeleted");
    }
}
