<?php

namespace Modules\Seguridad\Http\Livewire\Modulos;

use Livewire\Component;
use Illuminate\Support\Str;
use Modules\Seguridad\Entities\Modulo;

class Create extends Component
{
    public $model_id, $nombre, $icono;
    public $editing = false;

    public function render()
    {
        return view("seguridad::livewire.modulos.create");
    }

    public function resetInputFields()
    {
        $this->model_id = "";
        $this->nombre = "";
        $this->icono = "";
        $this->resetValidation();
    }
    public function store()
    {
        $this->validate([
            "nombre" => "required",
            "model_id" => "unique:modulos,id",
            "icono" => "required",
        ]);

        Modulo::create([
            "nombre" => $this->nombre,
            "id" => $this->model_id,
            "icono" => $this->icono,
        ]);

        $this->resetInputFields();

        $this->emit('moduloStored'); 
    }

    public function updatedNombre($value)
    {
    $this->model_id = Str::slug($value);
    }
}
