<?php

namespace Modules\Seguridad\Http\Livewire\Modulos;

use Livewire\Component;
use Illuminate\Support\Str;
use Modules\Seguridad\Entities\Modulo;

class Edit extends Component
{
    public $model_id, $nombre, $icono;
    public $editing = false;

    protected $listeners = [
        "moduloEdit" => "edit",
    ];

    public function render()
    {
        return view("seguridad::livewire.modulos.edit");
    }

    public function resetInputFields()
    {
        $this->model_id = "";
        $this->nombre = "";
        $this->icono = "";
        $this->resetValidation();
    }

    public function edit(Modulo $modulo)
    {
        $this->model_id = $modulo->id;
        $this->nombre = $modulo->nombre;
        $this->icono = $modulo->icono;
    }
    public function update()
    {
        $this->validate([
            "nombre" => "required",
            "icono" => "required",
        ]);

        Modulo::find($this->model_id)->update([
            "nombre" => $this->nombre,
            "icono" => $this->icono,
        ]);

        //$this->resetInputFields();

        $this->emit("moduloUpdated");
    }
}
