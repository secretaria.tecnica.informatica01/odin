<?php

namespace Modules\Seguridad\Http\Livewire\Roles;

use Livewire\Component;
use Modules\Seguridad\Entities\Modulo;
use Modules\Seguridad\Entities\Permission;
use Modules\Seguridad\Entities\Role;
use Illuminate\Validation\Rule;

class Create extends Component
{
    public $permisos = [];
    public $isSelectedAll = false;
    public $totalPermisos = 0;
    public $allPermisos = [];
    public $checks = [];
    public $allChecksTrue = [];
    public $allChecksFalse = [];
    public $modulos = [];

    public $name,
        $guard = "web",
        $model_id;

    public function render()
    {
        return view("seguridad::livewire.roles.create");
    }

    public function mount($rol = null)
    {
        $this->modulos = Modulo::with("permissions")->orderBy('orden')->get();
        $this->allPermisos = Permission::pluck("id")->toArray();
        $this->totalPermisos = count($this->allPermisos);

        $ids = $this->modulos->pluck("id")->toArray();
        $this->checks = array_fill_keys($ids, false);
        $this->allChecksFalse = $this->checks;
        $this->allChecksTrue = array_fill_keys($ids, true);

        if (isset($rol)) {
            $this->model_id = $rol->id;
            $this->name = $rol->name;
            $this->guard = $rol->guard_name;
            $permisos = $rol->permissions()->get();
            $this->permisos = $permisos->pluck("id")->toArray();

            #activar los modulos que almenos un permiso activo
            $modulos = $permisos->pluck("modulo_id", "modulo_id");
            foreach ($modulos as $modulo) {
                $this->checks[$modulo] = true;
            }

            $this->updatedPermisos();
        }
    }

    public function store()
    {
        $this->validate([
            "name" => [
                "required",
                Rule::unique("roles")->ignore($this->model_id),
            ],
            "guard" => "required",
        ]);

        $role = Role::firstOrNew(["id" => $this->model_id]);
        $role->name = $this->name;
        $role->guard_name = $this->guard;
        $role->save();
        $role->syncPermissions($this->permisos);

        return redirect()->route("seguridad.roles.list");
    }

    public function updatedPermisos()
    {
        $this->isSelectedAll = count($this->permisos) == $this->totalPermisos;
    }

    public function addModulos()
    {
    }

    public function updatedIsSelectedAll($value)
    {
        if ($this->isSelectedAll) {
            $this->permisos = $this->allPermisos;
            $this->checks = $this->allChecksTrue;
        } else {
            $this->permisos = [];
            $this->checks = $this->allChecksFalse;
        }
    }

    public function removePermisos($permisos)
    {
        $this->permisos = array_values(
            array_unique(array_diff($this->permisos, $permisos))
        );
        $this->updatedPermisos();
    }
    public function addPermisos($permisos)
    {
        $this->permisos = array_values(
            array_unique(array_merge($this->permisos, $permisos))
        );
        $this->updatedPermisos();
    }
}
