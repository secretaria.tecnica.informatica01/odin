<?php

namespace Modules\Seguridad\Http\Livewire\Roles;

use Livewire\Component;
use Modules\Seguridad\Entities\Role;
use Modules\Seguridad\Entities\Permission;

class Delete extends Component
{
    public function render()
    {
        return view('seguridad::livewire.roles.delete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Role::find($model_id)->delete();
        $this->emit("rolDeleted");
    }
}
