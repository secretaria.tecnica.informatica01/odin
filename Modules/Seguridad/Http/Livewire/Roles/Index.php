<?php

namespace Modules\Seguridad\Http\Livewire\Roles;

use Livewire\Component;
use Modules\Seguridad\Entities\Role;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "rolStored" => '$refresh',
        "rolUpdated" => '$refresh',
        "rolDeleted" => '$refresh',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $roles = Role::withCount('users')->where(
            "name",
            "like",
            "%" . $this->search . "%"
        )->paginate(10);
        return view("seguridad::livewire.roles.index", compact("roles"));
    }
}
