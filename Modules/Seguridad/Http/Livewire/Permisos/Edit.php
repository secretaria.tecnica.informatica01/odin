<?php

namespace Modules\Seguridad\Http\Livewire\Permisos;

use Livewire\Component;
use Illuminate\Support\Str;
use Modules\Seguridad\Entities\Permission;
use Illuminate\Validation\Rule;

class Edit extends Component
{
    public $model_id,
        $name,
        $label,
        $guard = "web";

    protected $listeners = [
        "permisoEdit" => "edit",
    ];

    public function render()
    {
        return view("seguridad::livewire.permisos.edit");
    }
    public function mount($modulo_id)
    {
        $this->modulo_id = $modulo_id;
    }

    public function resetInputFields()
    {
        $this->name = "";
        $this->label = "";
        $this->resetValidation();
    }
    public function edit(Permission $permiso)
    {
        $this->model_id = $permiso->id;
        $this->name = $permiso->name;
        $this->label = $permiso->label;
        $this->guard = $permiso->guard_name;
    }
    public function store()
    {
        $this->validate([
            "name" => [
                "required",
                Rule::unique("permissions")->ignore($this->model_id),
            ],
            "label" => "required",
            "guard" => "required",
        ]);

        Permission::find($this->model_id)->update([
            "name" => trim($this->name),
            "label" => trim($this->label),
            "guard_name" => $this->guard,
        ]);

        $this->resetInputFields();

        $this->emit("permisoUpdated");
    }

    public function updatedLabel($value)
    {
        $name = $this->modulo_id . " " . $value;
        $this->name = Str::of($name)
            ->ascii()
            ->lower();
    }
}
