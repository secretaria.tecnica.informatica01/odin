<?php

namespace Modules\Seguridad\Http\Livewire\Permisos;

use Livewire\Component;
use Modules\Seguridad\Entities\Modulo;
use Modules\Seguridad\Entities\Permission;

class Index extends Component
{
    public $modulo_id;

    protected $listeners = [
        "permisoStored" => '$refresh',
        "permisoUpdated" => '$refresh',
        "permisoDeleted" => '$refresh',
    ];

    public function render()
    {
        $permisos = Permission::where("modulo_id", $this->modulo_id)->get();
        return view("seguridad::livewire.permisos.index", compact("permisos"));
    }

    public function mount($modulo_id)
    {
        $this->modulo_id = $modulo_id;
    }
}
