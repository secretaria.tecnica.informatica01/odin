<?php

namespace Modules\Seguridad\Http\Livewire\Permisos;

use Livewire\Component;
use Illuminate\Support\Str;
use Modules\Seguridad\Entities\Permission;

class Create extends Component
{
    public $modulo_id;

    public $name,
        $label,
        $guard = "web";

    public function render()
    {
        return view("seguridad::livewire.permisos.create");
    }
    public function mount($modulo_id)
    {
        $this->modulo_id = $modulo_id;
    }

    public function resetInputFields()
    {
        $this->name = "";
        $this->label = "";
        $this->resetValidation();
    }
    public function store()
    {
        $this->validate([
            "name" => "required|unique:permissions",
            "label" => "required",
            "guard" => "required",
        ]);

        Permission::create([
            "name" => trim($this->name),
            "label" => trim($this->label),
            "modulo_id" => $this->modulo_id,
            "guard_name" => $this->guard,
        ]);

        $this->resetInputFields();

        $this->emit("permisoStored");
    }

    public function updatedLabel($value)
    {
        $name = $this->modulo_id . " " . $value;
        $this->name = Str::of($name)
            ->ascii()
            ->lower();
    }
}
