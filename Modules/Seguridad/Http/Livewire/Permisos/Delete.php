<?php

namespace Modules\Seguridad\Http\Livewire\Permisos;

use Livewire\Component;
use Modules\Seguridad\Entities\Permission;

class Delete extends Component
{
    public function render()
    {
        return view('seguridad::livewire.permisos.delete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Permission::find($model_id)->delete();
        $this->emit("permisoDeleted");
    }
}
