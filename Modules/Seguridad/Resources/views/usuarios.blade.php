@extends('layouts.contentLayoutMaster')
@section('title', 'Usuarios')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de usuarios</h3>
    <p class="mb-2">
      La cuenta de un usuario le permite a este autenticarse y tener acceso a este sistema
    </p>
  </div>
  
  <div class="col-auto">
    <button data-bs-target="#addUsuarioModal" data-bs-toggle="modal" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('seguridad::usuarios.index')
@livewire('seguridad::usuarios.create')
@livewire('seguridad::usuarios.edit')
@livewire('seguridad::usuarios.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection