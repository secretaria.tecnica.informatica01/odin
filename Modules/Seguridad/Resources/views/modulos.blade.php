@extends('layouts.contentLayoutMaster')
@section('title', 'Módulos')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de módulos</h3>
    <p class="mb-2">
      Un módulo constituye una funcionalidad o conjunto de funciones que son agrupadas por su estrecha relación.
    </p>
  </div>
  
  <div class="col-auto">
    <button data-bs-target="#addModuloModal" data-bs-toggle="modal" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('seguridad::modulos.index')
@livewire('seguridad::modulos.create')
@livewire('seguridad::modulos.edit')
@livewire('seguridad::modulos.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection
@section('page-script')
<!-- Page js files -->
<script type="text/javascript">
  window.livewire.on('moduloStored', () => {
    toastr['success']('', 'Módulo guardado', {
      closeButton: true,
      tapToDismiss: false,
      progressBar: true,
    });
  });
  window.livewire.on('moduloUpdated', () => {
    toastr['info']('', 'Módulo actualizado', {
      closeButton: true,
      tapToDismiss: false,
      progressBar: true,
    });
  });
</script>
@endsection