@extends('layouts.contentLayoutMaster')
@section('title', 'Permisos')

@section('vendor-style')
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de permisos para módulo: {{$modulo->nombre}}</h3>
    <p class="mb-2">
      Un módulo constituye una funcionalidad o conjunto de funciones que son agrupadas por su estrecha relación.
    </p>
  </div>
  
  <div class="col-auto">
    <button data-bs-target="#addPermisoModal" data-bs-toggle="modal" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('seguridad::permisos.index',['modulo_id'=>$modulo->id])
@livewire('seguridad::permisos.create',['modulo_id'=>$modulo->id])
@livewire('seguridad::permisos.edit',['modulo_id'=>$modulo->id])
@livewire('seguridad::permisos.delete')

@endsection

@section('vendor-script')
<!-- Vendor js files -->
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
@endsection