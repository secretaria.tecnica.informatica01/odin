<div>
    <div wire:ignore.self class="modal fade" id="addUsuarioModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Nuevo usuario</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-username">Usuario</label>
                            <input wire:model="username" id="new-username" type="text" name="username"
                                class="form-control @error('username') is-invalid @enderror"
                                placeholder="Ingrese el usuario" tabindex="1" />
                            @error('username') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-password">Contraseña</label>
                            <input wire:model="password" id="new-password" type="password" name="password"
                                class="form-control @error('password') is-invalid @enderror"
                                placeholder="Ingrese la contraseña" tabindex="1" />
                            @error('password') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>

                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-email">Correo electrónico</label>
                            <input wire:model="email" id="new-email" type="email" name="email"
                                class="form-control  @error('email') is-invalid @enderror"
                                placeholder="Ingrese correo electrónico del usuario" tabindex="2" />
                            @error('email') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-roles">Roles</label>
                                <select class="form-select" id="new-roles" multiple="multiple">
                                    <option></option>
                                    @foreach($roles as $rol)
                                    <option value="{{ $rol->name }}">{{ $rol->name }}</option>
                                    @endforeach
                                </select>
                                @error('roles') <span class="invalid-feedback">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-colaborador">Colaborador asociado</label>
                                <select class="form-select" id="new-colaborador">
                                    <option></option>
                                    @foreach($colaboradores as $col)
                                    <option value="{{ $col->id }}">{{ $col->nombres }} {{$col->apellido_p}}
                                        {{$col->apellido_m}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('colaborador_id') <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        $('#new-roles').select2({
            placeholder: "Selecione los roles"
        });

        $('#new-roles').on('change', function (e) {
            var data = $('#new-roles').select2("val");
            @this.set('selectedRoles', data);
        });
        @this.on('setRoles', (roles) => {
            $('#new-roles').val(roles).trigger('change');
        });

        $('#new-colaborador').select2({
            placeholder: "Selecione el colaborador",
            allowClear: true,
        });
        $('#new-colaborador').on('change', function (e) {
            var data = $('#new-colaborador').select2("val");
            @this.set('colaborador_id', data);
        });
        @this.on('setColaborador', (col) => {
            $('#new-colaborador').val(col).trigger('change.select2');
        });

        @this.on('usuarioStored', () => {
            toastr['success']('', 'Usuario guardado', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });
    });
</script>
@endpush