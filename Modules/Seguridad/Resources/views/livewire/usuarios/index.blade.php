<div>

    @livewire('seguridad::usuarios.statistics')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" name="search" class="form-control"
                            placeholder="Búsqueda de usuario">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$usuarios->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Foto</th>
                        <th>Nombre</th>
                        <th>Correo electrónico</th>
                        <th>Roles</th>
                        <th>Fecha de creación</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($usuarios as $index => $usuario)
                    <tr >
                        <td>
                            <img width="48" class="rounded-circle" src="{{$usuario->profile_photo_url}}" />
                        </td>
                        <td>
                            <span class="fw-bold">{{$usuario->name}}</span>
                        </td>

                        <td>
                            {{$usuario->email}}
                        </td>
                        <td>
                            @foreach ($usuario->roles as $rol)
                            <span class="badge rounded-pill badge-light-primary">
                                {{$rol->name}}
                            </span>
                            @endforeach
                        </td>

                        <td class="text-center">
                            <span title="{{$usuario->status}}" class="badge rounded-pill badge-light-{{$usuario->statusColor}}">
                                {{$usuario->created_at}}
                            </span>
                        </td>
                        <td>
                            <button data-bs-target="#editUsuarioModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('usuarioEdit','{{ $usuario->id }}')">
                                Editar</button>
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerDelete','{{ $usuario->id }}')">
                                Eliminar
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>