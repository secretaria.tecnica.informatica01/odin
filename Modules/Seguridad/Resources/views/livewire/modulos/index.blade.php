<div>
    <div class="row">
        @foreach ($modulos as $index => $modulo)
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-start">
                        <span>Total {{$modulo->permissions_count}} permisos</span>
                    </div>
                    <div class="d-flex mt-1 pt-25">
                        <div class="me-1">
                            <i class="{{$modulo->icono}} fa-4x"></i>
                        </div>
                        <div class="role-heading">
                            <h4 class="fw-bolder">{{$modulo->nombre}}</h4>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{route('seguridad.modulos.permisos', ['modulo' => $modulo->id]);}}" type="button" class="btn btn-sm btn-flat-secondary waves-effect">Permisos</a>
                        <button data-bs-target="#editModuloModal" data-bs-toggle="modal" type="button"
                            class="btn btn-sm btn-flat-primary waves-effect"
                            wire:click="$emit('moduloEdit','{{ $modulo->id }}')">Editar</button>
                        <button class="btn btn-sm btn-flat-danger waves-effect"
                            wire:click="$emit('triggerDelete','{{ $modulo->id }}')">
                            Eliminar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>