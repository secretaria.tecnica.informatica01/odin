<div>
    <div wire:ignore.self class="modal fade" id="editModuloModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Edición de módulo</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-nombre">Nombre de módulo</label>
                            <input wire:model="nombre" id="old-nombre" type="text" name="nombre"
                                class="form-control @error('nombre') is-invalid @enderror"
                                placeholder="Ingrese nombre de módulo" tabindex="1" />
                            @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-id">Identificador de módulo</label>
                            <input disabled wire:model="model_id" id="old-id" class="form-control" />
                        </div>
                        <div class="col-12">
                        <div class="d-flex justify-content-between">
                                <label class="form-label fw-bolder" for="old-icono">Ícono de módulo</label>
                                <a target="fontawesome" href="https://fontawesome.com/v5/search">Ejemplos</a>
                            </div>
                            <input wire:model="icono" id="old-icono" type="text" name="icono"
                                class="form-control  @error('icono') is-invalid @enderror"
                                placeholder="Ingrese ícono de módulo" tabindex="2" />
                            @error('icono') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="update()" class="btn btn-primary me-1">Actualizar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>