<div>
    <div class="card">
        <div class="card-body">
            <!-- Add role form -->
            <div class="row gy-1">
                <div class="col-md-6">
                    <label class="form-label fw-bolder" for="name">Nombre del rol</label>
                    <input id="name" wire:model="name" type="text" name="name"
                        class="form-control @error('name') is-invalid @enderror" placeholder="Ingrese el nombre del rol"
                        tabindex="1" />
                    @error('name') <span class="invalid-feedback">{{ $message }}</span> @enderror
                </div>
                <div class="col-md-6">
                    <label class="form-label fw-bolder" for="guard">Guardian</label>
                    <input id="guard" wire:model="guard" type="text" name="guard"
                        class="form-control @error('guard') is-invalid @enderror"
                        placeholder="Ingrese el nombre del guardian" tabindex="-1" />
                    @error('guard') <span class="invalid-feedback">{{ $message }}</span> @enderror
                </div>
                <div class="col-md-6">
                    <div class="form-check form-switch">
                        <input wire:model="isSelectedAll" type="checkbox" class="form-check-input" id="all-permissions">
                        <label class="form-check-label fw-bolder" for="all-permissions">Selecionar todos</label>
                    </div>
                    <div class="mt-1">Selecionar todos los permisos para este rol</div>
                </div>
                <div class="col-md-6 justify-content-end align-items-end d-flex">
                    <div>
                        <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                        <a class="btn btn-outline-secondary" href="{{route('seguridad.roles.list')}}">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>
            <!--/ Add role form -->
        </div>
    </div>

    <div class="row">
        @foreach ($modulos as $i => $modulo)
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row gy-1">
                        <div class="col-12">
                            <div class="form-check form-switch">
                                <input wire:click="$emit('moduloChecked','{{ $modulo->id }}',{{$checks[$modulo->id]}})"
                                    wire:model="checks.{{$modulo->id}}" type="checkbox" class="form-check-input"
                                    id="check-{{$modulo->id}}">
                                <label class="form-check-label fw-bolder"
                                    for="check-{{$modulo->id}}">{{$modulo->nombre}}</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <fieldset {{ $checks[$modulo->id] ? '' : 'disabled' }}>
                                <div class="row gy-1">
                                    <div class="col-12">
                                        <label class="form-check-label fw-bolder">Permisos disponibles:</label>
                                    </div>
                                    @foreach ($modulo->permissions as $j => $permiso)
                                    <div class="col-6">
                                        <div class="form-check form-check-inline">
                                            <input name="check-{{$modulo->id}}" wire:model="permisos"
                                                class="form-check-input" type="checkbox"
                                                id="check-{{$modulo->id}}-p-{{$permiso->id}}" value="{{$permiso->id}}">
                                            <label class="form-check-label"
                                                for="check-{{$modulo->id}}-p-{{$permiso->id}}">{{$permiso->label}}</label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </fieldset>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @section('page-script')
    <!-- Page js files -->
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            @this.on('moduloChecked', (modulo, value) => {
                var elementos = document.getElementsByName("check-" + modulo);
                var permisos = [];
                for (var check of elementos) {
                    permisos.push(check.value);
                }
                if (value == 1) {
                    @this.call('removePermisos', permisos);
                } else {
                    @this.call('addPermisos', permisos);
                }
            });
        })
    </script>
    @endsection
</div>