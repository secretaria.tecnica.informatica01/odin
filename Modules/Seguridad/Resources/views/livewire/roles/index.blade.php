<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda de rol">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$roles->links()}}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach ($roles as $index => $rol)
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <span>Total {{$rol->users_count}} usuarios</span>
                        <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                            @foreach ($rol->sampleUsers as $user)
                            <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                class="avatar avatar-sm pull-up">
                                <img class="rounded-circle" src="{{$user->profile_photo_url}}" />
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="d-flex mt-1 pt-25">
                        <div class="role-heading">
                            <h4 class="fw-bolder">{{$rol->name}}</h4>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{route('seguridad.roles.edit',['rol'=>$rol->id])}}" type="button" class="btn btn-sm
                            btn-flat-primary waves-effect">Editar</a>
                        <button class="btn btn-sm btn-flat-danger waves-effect"
                            wire:click="$emit('triggerDelete','{{ $rol->id }}')">
                            Eliminar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>