<div>
    <div wire:ignore.self class="modal fade" id="editPermisoModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Edición de permiso</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-label">Nombre del permiso</label>
                            <input wire:model="label" id="old-label" type="text" name="label"
                                class="form-control @error('label') is-invalid @enderror"
                                placeholder="Ingrese nombre de permiso" tabindex="1" />
                            @error('label') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-codigo">Código del permiso</label>
                            <input disabled wire:model="name" id="old-codigo" type="text"
                                class="form-control  @error('name') is-invalid @enderror"
                                placeholder="Ingrese un código único para el permiso" tabindex="-1" />
                            @error('name') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-guard">Guardia</label>
                            <input wire:model="guard" id="old-guard" type="text" name="guard"
                                class="form-control  @error('guard') is-invalid @enderror"
                                placeholder="Ingrese guardia para el permiso" tabindex="2" />
                            @error('guard') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            @this.on('permisoUpdated', () => {
                toastr['info']('', 'Permiso actualizado', {
                    closeButton: true,
                    tapToDismiss: false,
                    progressBar: true,
                });
                $('#editPermisoModal').modal('hide');
            });
        });
    </script>
    @endpush
</div>