<div class="card">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Código</th>
                    <th>Guardia</th>
                    <th>Fecha de creación</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($permisos as $index => $permiso)
                <tr>
                    <td>
                        <span class="fw-bold">{{$permiso->label}}</span>
                    </td>
                    <td>
                        {{$permiso->name}}
                    </td>

                    <td>
                        <span class="badge rounded-pill badge-light-primary">
                            {{$permiso->guard_name}}
                        </span>
                    </td>
                    <td>{{$permiso->created_at}}</td>
                    <td>
                        <button data-bs-target="#editPermisoModal" data-bs-toggle="modal" type="button"
                            class="btn btn-sm btn-flat-secondary waves-effect"
                            wire:click="$emit('permisoEdit','{{ $permiso->id }}')">
                            Editar</button>
                        <button class="btn btn-sm btn-flat-danger waves-effect"
                            wire:click="$emit('triggerDelete','{{ $permiso->id }}')">
                            Eliminar
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>