@extends('layouts.contentLayoutMaster')
@section('title', 'Nuevo rol')

@section('content')

@livewire('seguridad::roles.create')

@endsection