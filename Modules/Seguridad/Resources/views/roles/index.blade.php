@extends('layouts.contentLayoutMaster')
@section('title', 'Roles')

@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de roles</h3>
    <p class="mb-2">
        Un rol define un conjunto de actividades y la fuente de la información servicios que un usuario individual necesita para conseguir un desarrollo o un objetivo empresarial.
    </p>
  </div>
  
  <div class="col-auto">
    <a href="{{route('seguridad.roles.create')}}" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</a>
  </div>
</div>

@livewire('seguridad::roles.index')
@livewire('seguridad::roles.delete')

@endsection