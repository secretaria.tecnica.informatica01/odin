@extends('layouts.contentLayoutMaster')
@section('title', 'Edición de rol')

@section('content')

@livewire('seguridad::roles.create',['rol' => $rol])

@endsection