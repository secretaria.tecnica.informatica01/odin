<?php

namespace Modules\Seguridad\Entities;

use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    public function sampleUsers()
    {
        return $this->users()->limit(5);
    }
}
