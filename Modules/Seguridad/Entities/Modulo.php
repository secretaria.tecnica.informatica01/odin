<?php

namespace Modules\Seguridad\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Modulo extends Model
{
    use HasFactory;

    protected $keyType = "string";
    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = ["id", "nombre", "icono"];

    /**
     * Obtener los permisos disponibles en ese modulo
     */
    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
