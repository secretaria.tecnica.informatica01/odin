<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix("seguridad")
    ->name("seguridad.")
    ->middleware(["auth"])
    ->group(function () {
        Route::controller(SeguridadController::class)->group(function () {
            Route::get("/usuarios", "usuarios")->name("usuarios");
            Route::get("/modulos", "modulos")->name("modulos");
            Route::get("/modulos/{modulo}/permisos", "permisos")->name(
                "modulos.permisos"
            );
        });

        Route::controller(RolesController::class)
            ->prefix("roles")
            ->name("roles.")
            ->group(function () {
                Route::get("/", "index")->name("list");
                Route::get("/create", "create")->name("create");
                Route::post("/store", "store")->name("store");
                Route::get("/edit/{rol}/edit", "edit")->name("edit");
            });
    });
