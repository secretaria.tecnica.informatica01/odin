<?php

namespace Modules\Sam\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Sam\Entities\Actividad;
use Illuminate\Contracts\Support\Renderable;
use Modules\Sam\Http\Middleware\CanListHistoricoMiddleware;

class SamController extends Controller
{
    public function __construct()
    {
        $this->middleware("permission:sam actividad listar")->only(
            "actividades"
        );
        $this->middleware([
            "permission:sam historico listar",
            CanListHistoricoMiddleware::class,
        ])->only("historicos");
    }

    public function actividades()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Actividades"],
            ["name" => "Listado"],
        ];
        return view("sam::actividades", compact("breadcrumbs"));
    }

    public function unidades()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Unidades"],
            ["name" => "Listado"],
        ];
        return view("sam::unidades", compact("breadcrumbs"));
    }

    public function etiquetas()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Etiquetas"],
            ["name" => "Listado"],
        ];
        return view("sam::etiquetas", compact("breadcrumbs"));
    }

    public function solicitudes()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Solicitudes"],
            ["name" => "Listado"],
        ];
        return view("sam::solicitudes", compact("breadcrumbs"));
    }

    public function historicos(Actividad $actividad)
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            [
                "link" => "/sam/actividades",
                "name" => "Inicio",
                "name" => "Actividades",
            ],
            ["name" => $actividad->nombre],
            ["name" => "Historico"],
        ];
        return view("sam::historicos", compact("actividad", "breadcrumbs"));
    }
}
