<?php

namespace Modules\Sam\Http\Livewire\Historicos;

use Livewire\Component;
use App\Models\Revision;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Historico;

class Send extends Component
{
    public function render()
    {
        return view("sam::livewire.historicos.send");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Historico $historico)
    {
        //abrir transaccion
        DB::beginTransaction();

        $historico->estado = Historico::PENDING; //pendiente de revision

        $revisores = $historico->actividad->area->revisores_sam;

        $cantidad = count($revisores);
        if ($cantidad) {
            //desactivar las revisiones pasadas
            $historico->revisiones()->update([
                "estado" => Revision::CANCELLED,
            ]);

            $id = null;
            for ($i = $cantidad - 1; $i >= 0; $i--) {
                $revisor = $revisores[$i];
                $revision = new Revision();
                $revision->revisable_id = $historico->id;
                $revision->revisable_type = Historico::class;
                $revision->colaborador_id = $revisor->id; //usar id de revisor

                if ($i == 0) {
                    $revision->estado = Revision::PENDING;
                } else {
                    $revision->estado = Revision::WAITING;
                }

                $revision->revision_siguiente_id = $id;
                $revision->save();
                $id = $revision->id;
            }

            $historico->save();

            //actualizar totales sin el historico
            $actividad = $historico->actividad;
            $totales = $actividad
                ->historicos()
                ->select(
                    DB::raw(
                        "actividad_id, sum(cantidad) as t1, sum(cantidad_beneficiarios) as t2"
                    )
                )
                ->where("estado", Historico::CONFIRMED)
                ->groupBy("actividad_id")
                ->first();
            if (is_null($totales)) {
                $actividad->acomulado = 0;
                $actividad->acomulado_beneficiarios = 0;
            } else {
                $actividad->acomulado = $totales->t1;
                $actividad->acomulado_beneficiarios = $totales->t2;
            }
            $actividad->save();

            DB::commit();
        } else {
            DB::rollBack();
        }

        $this->emit("historicoUpdated");
    }
}
