<?php

namespace Modules\Sam\Http\Livewire\Historicos;

use App\Models\File;
use Livewire\Component;
use Livewire\WithPagination;
use Modules\Sam\Entities\Historico;
use Illuminate\Support\Facades\Storage;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "historicoStored" => '$refresh',
        "historicoUpdated" => '$refresh',
        "historicoDeleted" => '$refresh',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $historicos = Historico::with(
            "actividad",
            "actividad.area",
            "actividad.area.revisores_sam"
        )->where("actividad_id", $this->actividad_id);
        if (!empty($this->search)) {
            $historicos->where(
                "observaciones",
                "like",
                "%" . $this->search . "%"
            );
        }
        $historicos = $historicos->orderBy("id", "desc")->paginate(10);
        return view("sam::livewire.historicos.index", compact("historicos"));
    }

    public function mount($actividad_id)
    {
        $this->actividad_id = $actividad_id;
    }

    public function download(File $file)
    {
        return Storage::download($file->path,$file->file_name);
    }
}
