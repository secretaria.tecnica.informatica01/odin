<?php

namespace Modules\Sam\Http\Livewire\Historicos;

use Livewire\Component;

class Create extends Component
{
    public function render()
    {
        return view('sam::livewire.historicos.create');
    }
}
