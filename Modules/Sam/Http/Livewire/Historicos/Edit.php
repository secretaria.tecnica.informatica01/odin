<?php

namespace Modules\Sam\Http\Livewire\Historicos;

use Livewire\Component;
use App\Models\Revision;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Actividad;
use Modules\Sam\Entities\Historico;
use Modules\Sam\Services\UploadFileEvidenciaService;

class Edit extends Component
{
    use WithFileUploads;
    public $actividad;
    public $evidencia;
    public $adjuntos = [];
    public $revisores = 0;

    public $model_id;
    public $cantidad, $cantidad_beneficiados, $observaciones;
    public $removedFiles = [];
    protected $listeners = [
        "historicoEdit" => "edit",
    ];
    public function render()
    {
        return view("sam::livewire.historicos.edit");
    }

    public function edit(Historico $his)
    {
        $this->model_id = $his->id;
        $this->cantidad = $his->cantidad;
        $this->cantidad_beneficiarios = $his->cantidad_beneficiarios;
        $this->observaciones = $his->observaciones;
        $this->adjuntos = $his->adjuntos;
        $this->revisores =  count($his->actividad->area->revisores_sam);
    }

    public function mount(Actividad $actividad)
    {
        $this->actividad = $actividad;
    }

    protected function rules()
    {
        return [
            "cantidad" => "numeric",
            "cantidad_beneficiarios" => "numeric",
            "observaciones" => "",
        ];
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        $this->cantidad = 0;
        $this->cantidad_beneficiarios = 0;
        $this->observaciones = "";
        $this->evidencia = null;
    }

    public function update(UploadFileEvidenciaService $uploadService)
    {
        $this->validate();

        //abrir transaccion

        $his = Historico::find($this->model_id);

        DB::beginTransaction();

        $his->revisiones()->update([
            "estado" => Revision::CANCELLED,
        ]);

        $his->cantidad = $this->cantidad;
        $his->cantidad_beneficiarios = $this->cantidad_beneficiarios;
        $his->observaciones = $this->observaciones;
        $his->estado = Historico::DRAFT;

        if ($this->evidencia) {
            $file = $uploadService->handle($this->evidencia);
            $his->adjuntos()->save($file);
        }
        $his->save();

        //eliminar archivos vinculados
        if (count($this->removedFiles)) {
            $files = $his
                ->adjuntos()
                ->whereIn("id", $this->removedFiles)
                ->get();
            foreach ($files as $file) {
                $file->delete();
            }
        }
        DB::commit();
        $this->resetInputFields();
        $this->emit("historicoUpdated");
    }

    public function removeFile($file_id, $index)
    {
        $this->removedFiles[] = $file_id;
        unset($this->adjuntos[$index]);
    }
}
