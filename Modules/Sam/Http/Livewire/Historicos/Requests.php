<?php

namespace Modules\Sam\Http\Livewire\Historicos;

use Livewire\Component;
use Modules\Sam\Entities\Actividad;
use Modules\Sam\Entities\Historico;
class Requests extends Component
{
    public $revisiones = [];

    protected $listeners = [
        "historicoSolicitudes" => "show",
    ];
    public function render()
    {
        return view("sam::livewire.historicos.requests");
    }
    

    public function show(Historico $his)
    {
        $this->revisiones = $his->revisiones()->orderBy('created_at', 'desc')->get();
    }
}
