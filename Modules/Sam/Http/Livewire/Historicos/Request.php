<?php

namespace Modules\Sam\Http\Livewire\Historicos;

use App\Models\File;
use Livewire\Component;
use App\Models\Revision;
use Modules\Sam\Entities\Historico;
use Illuminate\Support\Facades\Storage;

class Request extends Component
{
    public $historico;
    public $solicitud;
    protected $listeners = [
        "requestHistorico" => "load",
    ];

    public function load(Revision $revision)
    {
        $this->revision = $revision;
        $this->historico = $revision->historico;
        $this->historico->load("actividad", "adjuntos");
    }

    public function render()
    {
        return view("sam::livewire.historicos.request");
    }

    public function download(File $file)
    {
        return Storage::download($file->path, $file->file_name);
    }
}
