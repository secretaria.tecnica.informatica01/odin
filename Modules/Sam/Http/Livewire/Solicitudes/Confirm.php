<?php

namespace Modules\Sam\Http\Livewire\Solicitudes;

use Livewire\Component;
use App\Models\Revision;
use Modules\Sam\Entities\Unidad;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Historico;

class Confirm extends Component
{
    public function render()
    {
        return view("sam::livewire.solicitudes.confirm");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirm(Revision $revision)
    {
        //abrir transaccion
        DB::beginTransaction();

        $revision->estado = Revision::CONFIRMED; //aceptado
        //solo para tipo historico
        //no existen mas revisiones que confirmar, puede darse por aprobado
        if (is_null($revision->revision_siguiente_id)) {
            //actualizar historico
            $revision->historico->estado = Historico::CONFIRMED;
            $revision->historico->save();

            //actualizar acomulados
            $actividad = $revision->historico->actividad;
            $totales = $actividad
                ->historicos()
                ->select(
                    DB::raw(
                        "actividad_id, sum(cantidad) as t1, sum(cantidad_beneficiarios) as t2"
                    )
                )
                ->where("estado", Historico::CONFIRMED)
                ->groupBy("actividad_id")
                ->first();
            if (is_null($totales)) {
                $actividad->acomulado = 0;
                $actividad->acomulado_beneficiarios = 0;
            } else {
                $actividad->acomulado = $totales->t1;
                $actividad->acomulado_beneficiarios = $totales->t2;
            }
            $actividad->save();
        } else {
            $revision->siguiente->estado = Revision::PENDING;
            $revision->siguiente->save();
        }
        $revision->save();

        DB::commit();
        $this->emit("solicitudConfirmed");
    }
}
