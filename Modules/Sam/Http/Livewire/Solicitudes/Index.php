<?php

namespace Modules\Sam\Http\Livewire\Solicitudes;

use Livewire\Component;
use App\Models\Revision;
use Livewire\WithPagination;
use Modules\Sam\Entities\Unidad;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "solicitudStored" => '$refresh',
        "solicitudConfirmed" => '$refresh',
        "solicitudRejected" => '$refresh',
    ];

    private function getSolicitudes()
    {
        $user = Auth::user();
        $colaborador = $user->colaborador;
        return $colaborador
            ->revisiones_sam()
            ->with(
                "historico",
                "historico.actividad",
                "historico.actividad.eje",
                "historico.actividad.programa",
                "historico.actividad.estrategia",
                "historico.actividad.area",
            )
            ->where("estado", Revision::PENDING); //solo activos
    }

    public function render()
    {
        $revisiones = $this->getSolicitudes()->paginate(10);
        return view("sam::livewire.solicitudes.index", compact("revisiones"));
    }
}
