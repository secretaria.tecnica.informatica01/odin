<?php

namespace Modules\Sam\Http\Livewire\Unidades;

use Livewire\Component;
use Modules\Sam\Entities\Unidad;
use Modules\Organizacion\Entities\Area;

class Create extends Component
{
    public $nombre, $descripcion, $model_id;

    protected function rules()
    {
        return [
            "nombre" => ["required"],
            "descripcion" => ["required"],
        ];
    }

    public function store()
    {
        $this->validate();

        Unidad::create([
            "nombre" => $this->nombre,
            "descripcion" => $this->descripcion,
        ]);

        $this->resetInputFields();

        $this->emit("unidadStored");
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        $this->nombre = "";
        $this->descripcion = "";
    }
    public function render()
    {
        return view("sam::livewire.unidades.create");
    }
}
