<?php

namespace Modules\Sam\Http\Livewire\Solicitudes;

use Livewire\Component;
use App\Models\Revision;
use Modules\Sam\Entities\Unidad;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Historico;

class Reject extends Component
{
    public function render()
    {
        return view("sam::livewire.solicitudes.reject");
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Revision $revision, $comentarios)
    {
        DB::beginTransaction();
        //cancelar otras revisiones
        $revision->historico->revisiones()->update([
            "estado" => Revision::CANCELLED,
        ]);
        $revision->estado = Revision::REJECTED; //rechazado
        $revision->comentarios = $comentarios;
        $revision->historico->estado = Historico::REJECTED;
        $revision->historico->save();
        $revision->save();
        DB::commit();
        $this->emit("solicitudRejected");
    }
}
