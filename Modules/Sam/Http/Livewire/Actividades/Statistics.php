<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use Livewire\Component;
use Modules\Sam\Entities\Etiqueta;

class Statistics extends Component
{
    protected $listeners = [
        "actividadStored" => '$refresh',
        "actividadDeleted" => '$refresh',
    ];

    public function render()
    {
        $etiquetas = Etiqueta::withCount("actividades")->get();
        return view(
            "sam::livewire.actividades.statistics",
            compact("etiquetas")
        );
    }
}
