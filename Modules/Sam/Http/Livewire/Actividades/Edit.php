<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Modules\Plan\Entities\Programa;
use Modules\Sam\Entities\Actividad;
use Modules\Plan\Entities\Estrategia;

class Edit extends Create
{
    protected $listeners = [
        "actividadEdit" => "edit",
    ];
    public function render()
    {
        return view("sam::livewire.actividades.edit");
    }

    public function edit(Actividad $act)
    {
        $this->model_id = $act->id;
        $this->eje_id = $act->eje_id;
        $this->programa_id = $act->programa_id;
        $this->estrategia_id = $act->estrategia_id;
        $this->area_id = $act->area_id;
        $this->unidad_id = $act->unidad_id;
        $this->nombre = $act->nombre;
        $this->descripcion = $act->descripcion;
        $this->alcance = $act->alcance;
        $this->etiquetas = $act->etiquetas->pluck("id");

        $this->emit("setArea", $act->area_id);
        $this->emit("setUnidad", $act->unidad_id);
        $this->emit("setEtiquetas", $this->etiquetas);

        $programas = Programa::where("eje_id", $this->eje_id)
            ->select(["id", "nombre as text"])
            ->get();
        $this->emit("setPrograma", $programas, $act->programa_id);

        $estrategias = Estrategia::where("programa_id", $this->programa_id)
            ->select(["id", "nombre as text"])
            ->get();
        $this->emit("setEstrategia", $estrategias, $act->estrategia_id);
    }

    public function update()
    {
        $this->validate();
        DB::beginTransaction();

        $act = Actividad::find($this->model_id);
        $act->eje_id = $this->eje_id;
        $act->programa_id = $this->programa_id;
        $act->estrategia_id = $this->estrategia_id;
        $act->area_id = $this->area_id;
        $act->unidad_id = $this->unidad_id;
        $act->nombre = $this->nombre;
        $act->descripcion = $this->descripcion;
        $act->alcance = $this->alcance;
        $act->etiquetas()->sync($this->etiquetas_id);
        $act->save();
        
        DB::commit();
        $this->resetInputFields();

        $this->emit("actividadUpdated");
    }
}
