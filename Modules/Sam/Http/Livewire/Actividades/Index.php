<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use Livewire\Component;
use Livewire\WithPagination;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Actividad;
use Illuminate\Support\Facades\Auth;
use Modules\Organizacion\Entities\Area;
use Modules\Sam\Exports\ActividadesExport;
use Modules\Sam\Imports\ActividadesImport;

class Index extends Filters
{
    use WithPagination;

    protected $listeners = [
        "actividadStored" => '$refresh',
        "actividadUpdated" => '$refresh',
        "actividadDeleted" => '$refresh',
        "actividadFiltrar" => '$refresh',
    ];

    public function updatingSearch()
    {
        $this->resetPage();
    }
    private function getOperator($operator = 0)
    {
        return ["=", ">", "<"][intval($operator)];
    }

    private function getQuery()
    {
        $actividades = Actividad::with([
            "area",
            "eje",
            "programa",
            "estrategia",
            "historico",
        ]);

        //solo se activan si se mandan fechas de busqueda
        if (!empty($this->inicio) || !empty($this->fin)) {
            $actividades
                ->withSum(
                    [
                        "historicos as acomulado" => function ($q) {
                            $q->desde($this->inicio);
                            $q->hasta($this->fin);
                        },
                    ],
                    "cantidad"
                )
                ->withSum(
                    [
                        "historicos as acomulado_beneficiarios" => function (
                            $q
                        ) {
                            $q->desde($this->inicio);
                            $q->hasta($this->fin);
                        },
                    ],
                    "cantidad_beneficiarios"
                );
        }

        //bloque de identificador
        if (is_array($this->ids) && count($this->ids)) {
            $actividades->whereIn("id", $this->ids);
        }

        //bloque de eje
        if ($this->eje_id > 0) {
            $actividades->where("eje_id", $this->eje_id);
        }

        //bloque de programa
        if ($this->programa_id > 0) {
            $actividades->where("programa_id", $this->programa_id);
        }

        //bloque de estrategia
        if ($this->estrategia_id > 0) {
            $actividades->where("estrategia_id", $this->estrategia_id);
        }

        //bloque de areas
        if ($this->area_id > 0) {
            if ($this->subareas) {
                $area = Area::find($this->area_id);
                $subareas = $area->all_areas_flat->pluck("id");
                $actividades->whereIn("area_id", $subareas);
            } else {
                $actividades->where("area_id", $this->area_id);
            }
        }

        //bloque de actividad
        if (!empty($this->actividad)) {
            $actividades->where("nombre", "like", "%" . $this->actividad . "%");
        }

        //bloque de descripcion
        if (!empty($this->descripcion)) {
            $actividades->where(
                "descripcion",
                "like",
                "%" . $this->descripcion . "%"
            );
        }

        //bloque de alcance
        if (!empty($this->alcance)) {
            $actividades->where("alcance", "like", "%" . $this->alcance . "%");
        }

        if (!empty($this->acomulado)) {
            $actividades->where(
                "acomulado",
                $this->getOperator($this->acomuladoDir),
                $this->acomulado
            );
        }

        if (!empty($this->beneficiariosAcomulado)) {
            $actividades->where(
                "acomulado_beneficiarios",
                $this->getOperator($this->bAcomuladoDir),
                $this->beneficiariosAcomulado
            );
        }

        //bloque de etiquetas
        if (is_array($this->etiquetas_id) && count($this->etiquetas_id)) {
            $actividades->whereHas("etiquetas", function ($q) {
                $q->whereIn("etiqueta_id", $this->etiquetas_id);
            });
        }

        //bloque de datos en historico
        if (!empty($this->cantidad)) {
            $actividades->whereHas("historico", function ($q) {
                $q->where(
                    "cantidad",
                    $this->getOperator($this->cantidadDir),
                    $this->cantidad
                );
            });
        }

        if (!empty($this->beneficiarios)) {
            $actividades->whereHas("historico", function ($q) {
                $q->where(
                    "cantidad_beneficiarios",
                    $this->getOperator($this->beneficiariosDir),
                    $this->beneficiarios
                );
            });
        }

        if (!empty($this->observaciones)) {
            $actividades->whereHas("historico", function ($q) {
                $q->where(
                    "observaciones",
                    "like",
                    "%" . $this->observaciones . "%"
                );
            });
        }

        if ($this->estado >= 0) {
            $actividades->whereHas("historico", function ($q) {
                $q->where("estado", $this->estado);
            });
        }

        //bloqueos por permisos

        $user = Auth::user();
        $colaborador = $user->colaborador;
        //bloqueos de ejes
        if ($user->can("sam actividad ejes")) {
            //sin bloqueos
        } else {
            $ejes = [0];
            if (isset($colaborador->area)) {
                $ejes = $colaborador->area->ejes_sam->pluck("id")->toArray();
            }
            $actividades->whereIn("eje_id", $ejes);
        }

        //bloqueos de programas
        if ($user->can("sam actividad programas")) {
            //sin bloqueos
        } else {
            $programas = [0];
            if (isset($colaborador->area)) {
                $programas = $colaborador->area->programas_sam
                    ->pluck("id")
                    ->toArray();
            }
            $actividades->whereIn("programa_id", $programas);
        }

        //bloqueos de estrategias
        if ($user->can("sam actividad estrategias")) {
            //sin bloqueos
        } else {
            $estra = [0];
            if (isset($colaborador->area)) {
                $estra = $colaborador->area->estrategias_sam
                    ->pluck("id")
                    ->toArray();
            }
            $actividades->whereIn("estrategia_id", $estra);
        }

        //bloqueos de areas
        if ($user->can("sam actividad areas")) {
            //no se aplica restriccion por area
        } elseif ($user->can("sam actividad subareas")) {
            //se aplica el filtro de cualquiermodo
            $subareas = [0];
            if (isset($colaborador->area)) {
                $area = $colaborador->area;
                $subareas = $area->all_areas_flat->pluck("id");
            }
            $actividades->whereIn("area_id", $subareas);
        } else {
            $actividades->where("area_id", $colaborador->area_id);
        }
        return $actividades;
    }
    public function render()
    {
        $query = $this->getQuery();
        $actividades = $query->paginate($this->num_filas);

        return view("sam::livewire.actividades.index", compact("actividades"));
    }

    public function mount()
    {
        parent::mount();
    }

    public function exportXLSX(Excel $excel)
    {
        $query = $this->getQuery();
        $actividades = new ActividadesExport($query);
        return $actividades->download("actividades.xlsx");
    }

    public function importarXLSX(Excel $excel)
    {
        $excel->import(new ActividadesImport, 'info.xlsx');
    }
}
