<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use Livewire\Component;
use Modules\Sam\Entities\Actividad;

class Delete extends Component
{
    public function render()
    {
        return view("sam::livewire.actividades.delete");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Actividad::find($model_id)->delete();
        $this->emit("actividadDeleted");
    }
}
