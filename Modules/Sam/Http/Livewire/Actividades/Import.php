<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use Livewire\Component;
use Modules\Sam\Entities\Etiqueta;

class Import extends Component
{
    public function render()
    {
        return view(
            "sam::livewire.actividades.import" );
    }
}
