<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use Carbon\Carbon;
use Livewire\Component;
use Illuminate\Support\Arr;
use Modules\Plan\Entities\Eje;
use Illuminate\Validation\Rule;
use Modules\Sam\Entities\Unidad;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Etiqueta;
use Modules\Plan\Entities\Programa;
use Modules\Sam\Entities\Actividad;
use Modules\Sam\Entities\Historico;
use Illuminate\Support\Facades\Auth;
use Modules\Plan\Entities\Estrategia;
use Modules\Organizacion\Entities\Area;

class Create extends Component
{
    public $ejes;
    public $areas;
    public $unidades;
    public $etiquetas = [];
    public $model_id,
        $eje_id = -1,
        $programa_id = -1,
        $estrategia_id = -1,
        $area_id = -1,
        $unidad_id = -1,
        $etiquetas_id = [],
        $descripcion,
        $alcance,
        $nombre;

    public function render()
    {
        return view("sam::livewire.actividades.create");
    }

    public function mount()
    {
        $user = Auth::user();
        $colaborador = $user->colaborador;

        $this->etiquetas = Etiqueta::pluck("nombre", "id")->toArray();
        $this->unidades = Unidad::pluck("nombre", "id")->toArray();

        //bloqueos de areas todo o solo la propia
        if ($user->can("sam actividad areas")) {
            $this->areas = Area::pluck("nombre", "id")->toArray();
        } /*elseif ($user->can("sam actividad subareas")) {
            $this->areas = $colaborador->areas
                ->pluck("nombre", "id")
                ->toArray();
        } */ else {
            $this->areas = Arr::pluck([$colaborador->area], "nombre", "id");
        }

        //bloqueos de ejes
        if ($user->can("sam actividad ejes")) {
            $this->ejes = Eje::pluck("nombre", "id")->toArray();
        } else {
            if (isset($colaborador->area)) {
                $this->ejes = $colaborador->area->ejes_sam
                    ->pluck("nombre", "id")
                    ->toArray();
            }
        }
    }

    public function updatedEjeId($value)
    {
        $user = Auth::user();
        $colaborador = $user->colaborador;
        $programas = [];

        //bloqueos de programas
        if ($user->can("sam actividad programas")) {
            $programas = Programa::where("eje_id", $this->eje_id)
                ->select(["id", "nombre as text"])
                ->get();
        } else {
            $programas = $colaborador->area
                ->programas_sam()
                ->where("eje_id", $this->eje_id)
                ->select(["id", "nombre as text"])
                ->get();
        }

        $this->programa_id = null;
        $this->estrategia_id = null;
        $this->emit("setPrograma", $programas, $this->programa_id);
        $this->emit("setEstrategia", [], $this->estrategia_id);
    }

    public function updatedProgramaId($value)
    {
        $user = Auth::user();

        $colaborador = $user->colaborador;
        $estrategias = [];

        //bloqueos de programas
        if ($user->can("sam actividad estrategias")) {//puede ver todas las estrategias
            $estrategias = Estrategia::where("programa_id", $this->programa_id)
                ->select(["id", "nombre as text"])
                ->get();
        } else {
            $estrategias = $colaborador->area
                ->estrategias_sam()
                ->where("programa_id", $this->programa_id)
                ->select(["id", "nombre as text"])
                ->get();
        }

        $this->estrategia_id = null;
        $this->emit("setEstrategia", $estrategias, $this->estrategia_id);
    }

    protected function rules()
    {
        return [
            "nombre" => [
                "required",
                Rule::unique("actividades")->ignore($this->model_id),
            ],
            "descripcion" => "required",
            "alcance" => "required",
            "eje_id" => "exists:ejes,id",
            "programa_id" => "required|exists:programas,id",
            "estrategia_id" => "required|exists:estrategias,id",
            "area_id" => "required|exists:areas,id",
            "unidad_id" => "required|exists:unidades,id",
        ];
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        //$act->eje_id = $this->eje_id;
        //$act->programa_id = $this->programa_id;
        //$act->estrategia_id = $this->estrategia_id;
        $this->area_id = -1;
        $this->unidad_id = -1;
        $this->nombre = "";
        $this->descripcion = "";
        $this->alcance = "";
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate();

        DB::beginTransaction();

        $act = new Actividad();
        $act->eje_id = $this->eje_id;
        $act->programa_id = $this->programa_id;
        $act->estrategia_id = $this->estrategia_id;
        $act->area_id = $this->area_id;
        $act->unidad_id = $this->unidad_id;
        $act->nombre = $this->nombre;
        $act->descripcion = $this->descripcion;
        $act->alcance = $this->alcance;
        $act->save();
        $act->etiquetas()->sync($this->etiquetas_id);

        $his = new Historico();
        $his->actividad_id = $act->id;
        $his->cantidad = 0;
        $his->cantidad_beneficiarios = 0;
        $his->inicio = Carbon::now();
        $his->estado = Historico::DRAFT;
        $his->activo = 1;
        $his->save();

        DB::commit();

        $this->resetInputFields();

        $this->emit("actividadStored");
    }
}
