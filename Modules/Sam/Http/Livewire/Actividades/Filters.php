<?php

namespace Modules\Sam\Http\Livewire\Actividades;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Arr;
use Modules\Plan\Entities\Eje;
use Modules\Sam\Entities\Etiqueta;
use Modules\Plan\Entities\Programa;
use Modules\Sam\Entities\Historico;
use Illuminate\Support\Facades\Auth;
use Modules\Plan\Entities\Estrategia;
use Modules\Organizacion\Entities\Area;

class Filters extends Component
{
    //filtros
    public $inicio;
    public $fin;
    public $ids;
    public $actividad;
    public $eje_id;
    public $programa_id;
    public $estrategia_id;
    public $area_id;
    public $observaciones;
    public $descripcion;
    public $alcance;
    public $etiquetas_id = [];
    public $cantidad;
    public $cantidadAcomulado;
    public $beneficiarios;
    public $beneficiariosAcomulado;
    public $subareas;
    public $estado = -1;

    public $cantidadDir;
    public $acomuladoDir;
    public $beneficiariosDir;
    public $bAcomuladoDir;

    public $num_filas = 10;

    protected $queryString = [
        "inicio" => ["except" => "", "as" => "in"],
        "fin" => ["except" => "", "as" => "fi"],
        "ids" => ["except" => "", "as" => "ids"],
        "actividad" => ["except" => "", "as" => "ac"],
        "eje_id" => ["except" => "", "as" => "ej"],
        "programa_id" => ["except" => "", "as" => "pr"],
        "estrategia_id" => ["except" => "", "as" => "es"],
        "area_id" => ["except" => "", "as" => "ar"],
        "observaciones" => ["except" => "", "as" => "ob"],
        "descripcion" => ["except" => "", "as" => "de"],
        "alcance" => ["except" => "", "as" => "al"],
        "etiquetas_id" => ["except" => "", "as" => "ei"],
        "subareas" => ["except" => false, "as" => "sa"],
        "estado" => ["except" => -1, "as" => "eo"],

        "cantidad" => ["except" => "", "as" => "ca"],
        "cantidadAcomulado" => ["except" => "", "as" => "cp"],
        "beneficiarios" => ["except" => "", "as" => "be"],
        "beneficiariosAcomulado" => ["except" => "", "as" => "bp"],

        "cantidadDir" => ["except" => "", "as" => "cd"],
        "acomuladoDir" => ["except" => "", "as" => "pd"],
        "beneficiariosDir" => ["except" => "", "as" => "bd"],
        "bAcomuladoDir" => ["except" => "", "as" => "bpd"],

        "num_filas" => ["except" => 10, "as" => "nf"],
    ];

    //datos para pintar
    public $etiquetas = [];
    public $ejes = [];
    public $areas = [];
    public $programas = [];
    public $estrategias = [];
    public $estados = [];

    public function render()
    {
        return view("sam::livewire.actividades.filters");
    }

    public function updatedEjeId($value)
    {
        $user = Auth::user();
        $colaborador = $user->colaborador;
        $programas = [];        

        //bloqueos de programas
        if ($user->can("sam actividad programas")) {
            $programas = Programa::where("eje_id", $this->eje_id)
                ->select(["id", "nombre as text"])
                ->get();
        } else {
            $programas = $colaborador->area
                ->programas_sam()
                ->where("eje_id", $this->eje_id)
                ->select(["id", "nombre as text"])
                ->get();
        }

        $this->programa_id = null;
        $this->estrategia_id = null;
        $this->emit("setPrograma", $programas, $this->programa_id);
        $this->emit("setEstrategia", [], $this->estrategia_id);
    }

    public function updatedProgramaId($value)
    {
        $user = Auth::user();

        $colaborador = $user->colaborador;
        $estrategias = [];

        //bloqueos de programas
        if ($user->can("sam actividad estrategias")) {//puede ver todas las estrategias
            $estrategias = Estrategia::where("programa_id", $this->programa_id)
                ->select(["id", "nombre as text"])
                ->get();
        } else {
            $estrategias = $colaborador->area
                ->estrategias_sam()
                ->where("programa_id", $this->programa_id)
                ->select(["id", "nombre as text"])
                ->get();
        }

        $this->estrategia_id = null;
        $this->emit("setEstrategia", $estrategias, $this->estrategia_id);
    }

    public function mount()
    {
        $user = Auth::user();
        $colaborador = $user->colaborador;
        $this->estados = Historico::ESTADOS;

        //bloqueos de areas
        if ($user->can("sam actividad areas")) {
            $this->areas = Area::pluck("nombre", "id")->toArray();
        } elseif ($user->can("sam actividad subareas")) {
            $this->areas = $colaborador->areas
                ->pluck("nombre", "id")
                ->toArray();
        } else {
            $this->areas = Arr::pluck([$colaborador->area], "nombre", "id");
        }

        //bloqueos de ejes
        if ($user->can("sam actividad ejes")) {
            $this->ejes = Eje::pluck("nombre", "id")->toArray();
        } else {
            if (isset($colaborador->area)) {
                $this->ejes = $colaborador->area->ejes_sam
                    ->pluck("nombre", "id")
                    ->toArray();
            }
        }

        //bloqueo de programas
        if ($this->eje_id > 0) {
            if ($user->can("sam actividad programas")) {
                $this->programas = Programa::where("eje_id", $this->eje_id)
                    ->pluck("nombre", "id")
                    ->toArray();
            } else {
                $this->programas = $colaborador->area
                    ->programas_sam()
                    ->where("eje_id", $this->eje_id)
                    ->pluck("nombre", "id")
                    ->toArray();
            }
        }
        //bloqueos de estrategias
        if ($this->programa_id > 0) {
            if ($user->can("sam actividad estrategias")) {
                $this->estrategias = Estrategia::where(
                    "programa_id",
                    $this->programa_id
                )
                    ->pluck("nombre", "id")
                    ->toArray();
            } else {
                $this->estrategias = $colaborador->area
                    ->estrategias_sam()
                    ->where("programa_id", $this->programa_id)
                    ->pluck("nombre", "id")
                    ->toArray();
            }
        }
        $this->etiquetas = Etiqueta::pluck("nombre", "id")->toArray();
    }
}
