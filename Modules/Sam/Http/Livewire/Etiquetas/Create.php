<?php

namespace Modules\Sam\Http\Livewire\Etiquetas;

use Livewire\Component;
use Illuminate\Validation\Rule;
use Modules\Sam\Entities\Unidad;
use Modules\Sam\Entities\Etiqueta;
use Modules\Organizacion\Entities\Area;

class Create extends Component
{
    public $nombre, $descripcion, $imagen, $color, $model_id;

    protected function rules()
    {
        return [
            "nombre" => ["required", Rule::notIn(["Otro", "Otros"])],
            "descripcion" => ["required"],
            "color" => ["required"],
            "imagen" => ["required"],
        ];
    }

    public function store()
    {
        $this->validate();

        Etiqueta::create([
            "nombre" => $this->nombre,
            "descripcion" => $this->descripcion,
            "color" => $this->color,
            "imagen" => $this->imagen,
        ]);

        $this->resetInputFields();

        $this->emit("unidadStored");
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        $this->nombre = "";
        $this->descripcion = "";
        $this->color = "";
        $this->imagen = "";
    }
    public function render()
    {
        return view("sam::livewire.etiquetas.create");
    }
}
