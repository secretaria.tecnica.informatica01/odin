<?php

namespace Modules\Sam\Http\Livewire\Etiquetas;

use Livewire\Component;
use Modules\Sam\Entities\Unidad;
use Modules\Sam\Entities\Etiqueta;
use Modules\Organizacion\Entities\Area;

class Edit extends Create
{
    protected $listeners = [
        "editUnidad" => "edit",
    ];

    public function edit(Etiqueta $etiqueta)
    {
        $this->nombre = $etiqueta->nombre;
        $this->descripcion = $etiqueta->descripcion;
        $this->imagen = $etiqueta->imagen;
        $this->color = $etiqueta->color;
        $this->model_id = $etiqueta->id;
    }

    public function update()
    {
        $this->validate();

        Etiqueta::find($this->model_id)->update([
            "nombre" => $this->nombre,
            "descripcion" => $this->descripcion,
            "imagen" => $this->imagen,
            "color" => $this->color,
        ]);

        $this->emit("unidadUpdated");
    }

    public function render()
    {
        return view("sam::livewire.etiquetas.edit");
    }
}
