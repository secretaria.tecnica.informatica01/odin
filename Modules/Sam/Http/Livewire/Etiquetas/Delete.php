<?php

namespace Modules\Sam\Http\Livewire\Etiquetas;

use Livewire\Component;
use Modules\Sam\Entities\Unidad;
use Modules\Sam\Entities\Etiqueta;

class Delete extends Component
{
    public function render()
    {
        return view("sam::livewire.etiquetas.delete");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Etiqueta::find($model_id)->delete();
        $this->emit("unidadDeleted");
    }
}
