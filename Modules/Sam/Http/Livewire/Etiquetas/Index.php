<?php

namespace Modules\Sam\Http\Livewire\Etiquetas;

use Livewire\Component;
use Livewire\WithPagination;
use Modules\Sam\Entities\Unidad;
use Modules\Sam\Entities\Etiqueta;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "unidadStored" => '$refresh',
        "unidadUpdated" => '$refresh',
        "unidadDeleted" => '$refresh',
    ];

    private function getEtiquetas()
    {
        //filtrado por query
        return Etiqueta::withCount("actividades")->where(function ($q) {
            $q->where("nombre", "like", "%" . $this->search . "%")->orWhere(
                "descripcion",
                "like",
                "%" . $this->search . "%"
            );
        });
    }

    public function render()
    {
        $etiquetas = $this->getEtiquetas()->paginate(10);
        return view("sam::livewire.etiquetas.index", compact("etiquetas"));
    }
}
