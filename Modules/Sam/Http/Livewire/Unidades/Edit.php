<?php

namespace Modules\Sam\Http\Livewire\Unidades;

use Livewire\Component;
use Modules\Sam\Entities\Unidad;
use Modules\Organizacion\Entities\Area;

class Edit extends Create
{
    protected $listeners = [
        "editUnidad" => "edit",
    ];

    public function edit(Unidad $unidad)
    {
        $this->nombre = $unidad->nombre;
        $this->descripcion = $unidad->descripcion;
        $this->model_id = $unidad->id;
    }

    public function update()
    {
        $this->validate();

        Unidad::find($this->model_id)->update([
            "nombre" => $this->nombre,
            "descripcion" => $this->descripcion,
        ]);

        $this->emit("unidadUpdated");
    }

    public function render()
    {
        return view("sam::livewire.unidades.edit");
    }
}
