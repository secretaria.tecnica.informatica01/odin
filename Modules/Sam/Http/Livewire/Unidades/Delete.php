<?php

namespace Modules\Sam\Http\Livewire\Unidades;

use Livewire\Component;
use Modules\Sam\Entities\Unidad;

class Delete extends Component
{
    public function render()
    {
        return view("sam::livewire.unidades.delete");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Unidad::find($model_id)->delete();
        $this->emit("unidadDeleted");
    }
}
