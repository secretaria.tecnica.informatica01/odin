<?php

namespace Modules\Sam\Http\Livewire\Unidades;

use Livewire\Component;
use Livewire\WithPagination;
use Modules\Sam\Entities\Unidad;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ["search"];

    protected $listeners = [
        "unidadStored" => '$refresh',
        "unidadUpdated" => '$refresh',
        "unidadDeleted" => '$refresh',
    ];

    private function getUnidades()
    {
        //filtrado por query
        return Unidad::withCount("actividades")->where(function ($q) {
            $q->where("nombre", "like", "%" . $this->search . "%")->orWhere(
                "descripcion",
                "like",
                "%" . $this->search . "%"
            );
        });
    }

    public function render()
    {
        $unidades = $this->getUnidades()->paginate(10);
        return view("sam::livewire.unidades.index", compact("unidades"));
    }
}
