<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Actividades extends Component
{
    public function render()
    {
        $dato = DB::table('actividades')->count();
        return view("sam::livewire.widgets.actividades", compact("dato"));
    }
}
