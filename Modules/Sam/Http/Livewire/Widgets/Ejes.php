<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Ejes extends Component
{
    public function render()
    {
        $dato = DB::table('ejes')->count();
        return view("sam::livewire.widgets.ejes", compact("dato"));
    }
}
