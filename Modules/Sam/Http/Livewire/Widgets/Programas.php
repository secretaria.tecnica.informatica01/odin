<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Programas extends Component
{
    public function render()
    {
        $dato = DB::table('programas')->count();
        return view("sam::livewire.widgets.programas", compact("dato"));
    }
}
