<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Areas extends Component
{
    public function render()
    {
        $dato = DB::table('areas')->count();
        return view("sam::livewire.widgets.areas", compact("dato"));
    }
}
