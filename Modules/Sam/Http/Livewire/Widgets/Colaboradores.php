<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Colaboradores extends Component
{
    public function render()
    {
        $dato = DB::table('colaboradores')->count();
        return view("sam::livewire.widgets.colaboradores", compact("dato"));
    }
}
