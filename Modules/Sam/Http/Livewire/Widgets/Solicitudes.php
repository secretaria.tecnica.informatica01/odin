<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use App\Models\Revision;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Solicitudes extends Component
{
    public function render()
    {
        $dato = $user = Auth::user();
        $dato = 0;
        if (!is_null($user->colaborador)) {
            $colaborador = $user->colaborador;
            $dato = $colaborador
                ->revisiones_sam()
                ->where("estado", Revision::PENDING) //solo activos;
                ->count();
        }
        return view("sam::livewire.widgets.solicitudes", compact("dato"));
    }
}
