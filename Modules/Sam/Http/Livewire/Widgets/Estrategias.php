<?php

namespace Modules\Sam\Http\Livewire\Widgets;

use Livewire\Component;
use Illuminate\Support\Facades\DB;

class Estrategias extends Component
{
    public function render()
    {
        $dato = DB::table('estrategias')->count();
        return view("sam::livewire.widgets.estrategias", compact("dato"));
    }
}
