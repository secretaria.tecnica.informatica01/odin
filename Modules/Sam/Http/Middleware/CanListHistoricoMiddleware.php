<?php

namespace Modules\Sam\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Sam\Entities\Actividad;
use Illuminate\Support\Facades\Auth;

class CanListHistoricoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        $colaborador = $user->colaborador;
        $actividad = $request->route("actividad");
        if ($user->can("sam actividad areas")) {
            //no se aplica restriccion por area
        } elseif ($user->can("sam actividad subareas")) {
            $area = $colaborador->area;
            $subareas = $area->all_areas_flat->pluck("id")->toArray();
            //esta area no esta en sus subareas
            if (!in_array($actividad->area_id, $subareas)) {
                abort(403, "Access denied");
            }
        } else {
            //no es su area
            if ($actividad->area_id != $colaborador->area_id) {
                abort(403, "Access denied");
            }
        }
        //abort(403, "Access denied");
        return $next($request);
    }
}
