<?php
namespace Modules\Sam\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class ActividadesExport implements
    FromCollection,
    WithMapping,
    WithHeadings,
    WithStyles,
    WithColumnWidths
{
    use Exportable;

    private $query;
    public function __construct($query)
    {
        $this->query = $query;
    }

    public function collection()
    {
        return $this->query->get();
    }
    public function map($actividad): array
    {
        $eje = "";
        $programa = "";
        $estrategia = "";
        $area = "";
        $observaciones = "";
        $inicio = "";
        $fin = "";
        $estado = "";
        $parcial1 = "";
        $parcial2 = "";

        if (!is_null($actividad->historico)) {
            $inicio = $actividad->historico->inicio;
            $fin = $actividad->historico->fin;
            $estado = $actividad->historico->status;
            $parcial1 = $actividad->historico->cantidad;
            $parcial2 = $actividad->historico->cantidad_beneficiarios;
        }

        if (!is_null($actividad->eje)) {
            $eje = $actividad->eje->nombre;
        }

        if (!is_null($actividad->programa)) {
            $programa = $actividad->programa->nombre;
        }

        if (!is_null($actividad->estrategia)) {
            $estrategia = $actividad->estrategia->nombre;
        }

        if (!is_null($actividad->area)) {
            $area = $actividad->area->nombre;
        }
        return [
            $actividad->id,
            $inicio,
            $fin,
            $eje,
            $programa,
            $estrategia,
            $area,
            $actividad->nombre,
            $actividad->descripcion,
            $actividad->alcance,
            $actividad->acomulado,
            $actividad->acomulado_beneficiarios,
            $parcial1,
            $parcial2,
            $estado,
        ];
    }

    public function headings(): array
    {
        return [
            "ID",
            "Inicio",
            "Fin",
            "Eje",
            "Programa",
            "Estrategia",
            "Área",
            "Actividad",
            "Descripción",
            "Alcance",
            "Cantidad acomulada",
            "Beneficiarios acomulado",
            "Cantidad parcial",
            "Beneficiarios parcial",
            "Estado",
        ];
    }
    public function columnWidths(): array
    {
        return [
            "H" => 50,
            "I" => 50,
            "J" => 50,
        ];
    }
    public function styles(Worksheet $sheet)
    {
        $sheet
            ->getStyle("H:J", $sheet->getHighestRow())
            ->getAlignment()
            ->setWrapText(true);
    }
}
