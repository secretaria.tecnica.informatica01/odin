<?php

namespace Modules\Sam\Console;

use Illuminate\Console\Command;
use Modules\Sam\Jobs\CutoffActividadesJob;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CutoffActividadesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'cutoff-actividades';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Realizar corte a las actividades';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        CutoffActividadesJob::dispatch();
    }
}
