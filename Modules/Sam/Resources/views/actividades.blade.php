@extends('layouts.contentLayoutMaster')
@section('title', 'Actividades')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h3>Listado de actividades</h3>
        <p class="mb-2">
            Son las principales acciones y recursos
            asignados para producir cada uno de los componentes.
        </p>
    </div>

    <div class="col-auto">
        <button data-bs-target="#addActividadModal" data-bs-toggle="modal"
            class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nueva</button>
    </div>
</div>

@livewire('sam::actividades.index')
@livewire('sam::actividades.create')
@livewire('sam::actividades.edit')
@livewire('sam::actividades.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection