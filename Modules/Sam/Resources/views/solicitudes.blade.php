@extends('layouts.contentLayoutMaster')
@section('title', 'Solicitudes')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de solicitudes</h3>
    <p class="mb-2">
      Una solicitud consiste en un informe el cual requiere de su revision para aprobación o rechazo.
    </p>
  </div>
  
  <div class="col-auto">
    <button onclick="window.livewire.emit('createUnidad');" click data-bs-target="#addUnidadModal" data-bs-toggle="modal" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('sam::solicitudes.index')
@livewire('sam::historicos.request')
@livewire('sam::solicitudes.reject')
@livewire('sam::solicitudes.confirm')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection