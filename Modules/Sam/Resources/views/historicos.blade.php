@extends('layouts.contentLayoutMaster')
@section('title', 'Históricos')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
    <div class="col">
        <h3>Listado de históricos</h3>
        <p class="mb-2">
            Son las principales acciones y recursos
            asignados para producir cada uno de los componentes.
        </p>
    </div>

    <div class="col-auto">
    </div>
</div>

@livewire('sam::historicos.index',[$actividad->id])
@livewire('sam::historicos.edit',[$actividad->id])
@livewire('sam::historicos.requests')
@livewire('sam::historicos.send')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection