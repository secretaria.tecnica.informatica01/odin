<div>
    <div wire:ignore.self class="modal fade" id="editHistoricoModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Detalle solicitud</h2>
                    </div>
                    @if(isset($historico))
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder">Nombre de actividad</label>
                            <input disabled class="form-control" value="{{$historico->actividad->nombre}}" />
                        </div>
                        <div class="col-6">
                            <label class="form-label fw-bolder">Cantidad</label>
                            <input disabled class="form-control" value="{{$historico->cantidad}}" />
                        </div>
                        <div class="col-6">
                            <label class="form-label fw-bolder">Beneficiarios</label>
                            <input disabled class="form-control" value="{{$historico->cantidad_beneficiarios}}" />
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder">Observaciones</label>
                            <textarea rows="5" class="form-control" disabled>{{$historico->observaciones}}</textarea>
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder">Evidencias</label>
                            <ul class="list-group list-group-numbered">
                                @foreach($historico->adjuntos as $index => $file)
                                <li class="list-group-item d-flex align-items-center">
                                    <span class="flex-fill">&nbsp;{{$file->file_name}}</span>
                                    <div>
                                        <button wire:click.prevent="download({{$file->id}})"  type="button" class="btn btn-flat-primary waves-effect btn-sm "><i class="fas fa-download"></i></button>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="$emit('triggerConfirm','{{ $revision->id }}')" class="btn btn-primary me-1">Aprobar</button>
                            <button wire:click.prevent="$emit('triggerReject','{{$revision->id}}')" class="btn btn-danger me-1">Rechazar</button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>