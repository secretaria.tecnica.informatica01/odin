<div>
    <div wire:ignore.self class="modal fade" id="requestsHistoricoModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Historial de revisiones</h2>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Revisor</th>
                                        <th>Comentarios</th>
                                        <th>Resultado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($revisiones as $index => $revision)
                                        <tr>
                                            <td>
                                                {{$revision->created_at}}
                                            </td>
                                            <td>
                                                {{$revision->revisor->full_name}}
                                            </td>
                                            <td>
                                                {{$revision->comentarios}}
                                            </td>
                                            <td>
                                                {{$revision->status}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

