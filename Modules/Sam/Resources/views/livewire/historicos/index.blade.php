<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda por observaciones">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$historicos->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Inicio</th>
                        <th>Fin</th>
                        <th>Cantidad</th>
                        <th>Beneficiados</th>
                        <th>Observaciones</th>
                        <th>Evidencias</th>
                        <th>Revisores</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($historicos as $index => $historico)
                    <tr>
                        <td @if($historico->activo) class="table-success" @endif>
                            {{$historico->inicio}}
                        </td>
                        <td @if($historico->activo) class="table-success" @endif>
                            @if(empty($historico->fin))
                            Hoy
                            @else
                            {{$historico->fin}}
                            @endif
                        </td>
                        <td>
                            {{$historico->cantidad}}
                        </td>
                        <td>
                            {{$historico->cantidad_beneficiarios}}
                        </td>
                        <td>
                            {{$historico->observaciones}}
                        </td>
                        <td>
                            <ol class="p-0 mb-0">
                                @foreach($historico->adjuntos as $index => $file)
                                <li>
                                    <a wire:click="download({{$file->id}})" href="#">{{$file->file_name}}</a>
                                </li>
                                @endforeach
                            </ol>
                        </td>
                        <td>
                            <ol class="p-0 mb-0">
                                @forelse($historico->actividad->area->revisores_sam as $index => $revisor)
                                <li>
                                    {{$revisor->full_name}}
                                </li>@empty
                                <p>Área sin revisores</p>
                                @endforelse
                            </ol>
                        </td>
                        <td class="{{$historico->color}}">
                            {{$historico->status}}
                        </td>
                        <td>
                            @if($historico->activo || Auth::user()->can('sam historico editar inactivos'))
                            <button data-bs-target="#editHistoricoModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('historicoEdit','{{ $historico->id }}')">
                                Editar</button>
                            @endif
                            @if(count($historico->actividad->area->revisores_sam))
                            <div>
                                <button type="button" class="btn btn-sm btn-flat-success waves-effect"
                                    wire:click="$emit('triggerSend','{{ $historico->id }}')">
                                    Enviar</button>
                            </div>
                            @endif
                            <button data-bs-target="#requestsHistoricoModal" data-bs-toggle="modal" type="button"
                            class="btn btn-sm btn-flat-info waves-effect"
                            wire:click="$emit('historicoSolicitudes','{{ $historico->id }}')">
                            Solicitudes</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>