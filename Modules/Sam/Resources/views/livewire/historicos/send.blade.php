<div>
    @push('scripts')
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            window.livewire.on('triggerSend', model_id => {
                Swal.fire({
                    title: '¿Esta seguro de mandar a revisión?',
                    text: 'La solicitud debera ser aprobada por todos los revisores',
                    icon: "info",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    confirmButtonText: 'Enviar',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    //if user clicks on delete
                    if (result.value) {
                        // calling destroy method to delete
                        @this.call('send', model_id);
                        // success response
                        Swal.fire({ title: '¡Información enviada a revisión!', icon: 'success' });
                    }
                });
            });
        })
    </script>
    @endpush
</div>