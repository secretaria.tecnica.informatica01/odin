<div>
    <div wire:ignore.self class="modal fade" id="editHistoricoModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Editar histórico</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder">Nombre de actividad</label>
                            <input disabled class="form-control" value="{{$actividad->nombre}}" />
                        </div>
                        <div class="col-6">
                            <label class="form-label fw-bolder" for="new-cantidad">Cantidad</label>
                            <input wire:model="cantidad" id="new-cantidad" type="text" name="cantidad"
                                class="form-control @error('cantidad') is-invalid @enderror"
                                placeholder="Ingrese la cantidad de unidades" tabindex="1" />
                            @error('cantidad') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-6">
                            <label class="form-label fw-bolder" for="new-cantidad_beneficiarios">Beneficiados</label>
                            <input wire:model="cantidad_beneficiarios" id="new-cantidad_beneficiarios" type="text"
                                name="cantidad_beneficiarios"
                                class="form-control @error('cantidad_beneficiarios') is-invalid @enderror"
                                placeholder="Ingrese cantidad_beneficiarios de programa de desarrollo" tabindex="1" />
                            @error('cantidad_beneficiarios') <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-observaciones">Observaciones</label>
                            <textarea rows="5" wire:model="observaciones" id="new-observaciones" type="text"
                                class="form-control  @error('observaciones') is-invalid @enderror"
                                placeholder="Ingrese un índice para el programa" tabindex="-1"></textarea>
                            @error('observaciones') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-evidencia">Evidencias</label>
                            <input wire:model="evidencia" class="form-control @error('evidencias') is-invalid @enderror"
                                type="file" id="new-evidencia" />
                        </div>
                        <div class="col-12">
                            <ul class="list-group list-group-numbered">
                                @foreach($adjuntos as $index => $file)
                                <li class="list-group-item d-flex align-items-center">
                                    <span class="flex-fill">&nbsp;{{$file->file_name}}</span>
                                    <div>
                                        <button wire:click.prevent="removeFile({{$file->id}},{{$index}})" type="button"
                                            class="btn btn-flat-danger waves-effect btn-sm "><i
                                                class="fas fa-times"></i></button>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-12 text-center mt-2">

                            @if($revisores)
                            <button wire:click.prevent="$emit('triggerSend','{{ $model_id }}')"
                                class="btn btn-success me-1">Enviar a revisión</button>
                            @endif
                            <button wire:click.prevent="update()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        @this.on('historicoUpdated', () => {
            $('#editHistoricoModal').modal('hide');
            toastr['info']('', 'Histórico actualizado', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });
    });
</script>
@endpush