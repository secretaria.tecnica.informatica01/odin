<div>
    @push('scripts')
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            window.livewire.on('triggerReject', model_id => {
                let modal = $("#editHistoricoModal");
                if(modal.length){
                    modal.modal('hide');
                }
                Swal.fire({
                    title: '¿Esta seguro de rechazar?',
                    input:"textarea",
                    text: 'Puede agregar comentarios o sugerencias',
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    confirmButtonText: 'Rechazar',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    //if user clicks on delete
                    if (result.isConfirmed) {
                        // calling destroy method to delete
                        @this.call('reject', model_id, result.value);
                        // success response
                        Swal.fire({ title: '¡Solicitud rechazada correctamente!', icon: 'success' });
                    }
                });
            });
        })
    </script>
    @endpush
</div>