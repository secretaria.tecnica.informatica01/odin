<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$revisiones->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-striped table-sm table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fecha</th>
                        <th>Eje</th>
                        <th>Área</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Alcance</th>
                        <th>Cantidad</th>
                        <th>Beneficiarios</th>
                        <th>Observaciones y otros</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($revisiones as $revision)
                    <tr>
                        <td>
                            {{$revision->historico->actividad_id}}
                        </td>
                        <td>
                            {{$revision->created_at}}
                        </td>
                        <td>
                            {{$revision->historico->actividad->eje->nombre}}
                        </td>
                        <td>
                            {{$revision->historico->actividad->area->nombre}}
                        </td>
                        <td>
                            {{$revision->historico->actividad->nombre}}
                        </td>
                        <td>
                            {{$revision->historico->actividad->descripcion}}
                        </td>
                        <td>
                            {{$revision->historico->actividad->alcance}}
                        </td>
                        <td>
                            {{$revision->historico->cantidad}}
                        </td>
                        <td>
                            {{$revision->historico->cantidad_beneficiarios}}
                        </td>
                        <td>
                            {{$revision->historico->observaciones}}
                        </td>
                        <td>
                            <button data-bs-target="#editHistoricoModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-secondary waves-effect"
                                wire:click="$emit('requestHistorico','{{ $revision->id }}')">
                                Detalle</button>
                            <button type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('triggerConfirm','{{ $revision->id }}')">
                                Aprobar</button>
                            @if($revision->actividades_count == 0)
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerReject','{{ $revision->id }}')">
                                Rechazar
                            </button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>