<div>
    @push('scripts')
    <script type="text/javascript">
        document.addEventListener('livewire:load', function () {
            window.livewire.on('triggerConfirm', model_id => {
                let modal = $("#editHistoricoModal");
                if(modal.length){
                    modal.modal('hide');
                }
                Swal.fire({
                    title: '¿Esta seguro de aprobar?',
                    text: 'La solicitud será mostrada como validada por usted',
                    icon: "info",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#aaa',
                    confirmButtonText: 'Aprobar',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    //if user clicks on delete
                    if (result.isConfirmed) {
                        // calling accept method to accept
                        @this.call('confirm', model_id);
                        // success response
                        Swal.fire({ title: '¡Solicitud aprobada correctamente!', icon: 'success' });
                    }
                });
            });
        })
    </script>
    @endpush
</div>