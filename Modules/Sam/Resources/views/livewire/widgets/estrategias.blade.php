
<div class="card text-center">
          <div class="card-body">
              <div class="avatar bg-light-danger p-50 mb-1">
                  <div class="avatar-content">
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck font-medium-5"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>
                  </div>
              </div>
        <h2 class="fw-bolder">{{$dato}}</h2>
              <p class="card-text">Estrategias</p>
          </div>
      </div>