<div class="card text-center">
          <div class="card-body">
              <div class="avatar bg-light-warning p-50 mb-1">
                  <div class="avatar-content">
                      <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square font-medium-5"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg>
                  </div>
              </div>
              <h2 class="fw-bolder">{{$dato}}</h2>
        <p class="card-text">Áreas</p>
          </div>
      </div>