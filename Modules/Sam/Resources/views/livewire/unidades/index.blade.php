<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$unidades->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Cantidad usos</th>
                        <th>Fecha de creación</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($unidades as $unidad)
                    <tr>
                        <td>
                            {{$unidad->id}}
                        </td>
                        <td>
                            {{$unidad->nombre}}
                        </td>
                        <td>
                            {{$unidad->descripcion}}
                        </td>
                        <td>
                            {{$unidad->actividades_count}}
                        </td>
                        <td>
                            {{$unidad->created_at}}
                        </td>
                        <td>
                            <button data-bs-target="#editUnidadModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('editUnidad','{{ $unidad->id }}')">
                                Editar</button>
                            @if($unidad->actividades_count == 0)
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerDelete','{{ $unidad->id }}')">
                                Eliminar
                            </button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>