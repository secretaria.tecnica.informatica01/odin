<div>
    <div wire:ignore.self class="modal fade" id="editUnidadModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Editar unidad</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-nombre">Nombre</label>
                            <input wire:model="nombre" id="old-nombre" type="text"
                                class="form-control @error('nombre') is-invalid @enderror"
                                placeholder="Ingrese nombre para el área" tabindex="1" />
                            @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="old-descripcion">Descripción</label>
                            <textarea rows="5" wire:model="descripcion" id="old-descripcion" type="text"
                                class="form-control  @error('descripcion') is-invalid @enderror"
                                placeholder="Ingrese un índice para el programa" tabindex="-1"></textarea>
                            @error('descripcion') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="update()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        @this.on('unidadUpdated', () => {
            $('#editUnidadModal').modal('hide');
            toastr['info']('', 'Unidad actualizada', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });

    });
</script>
@endpush