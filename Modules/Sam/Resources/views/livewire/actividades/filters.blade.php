<div class="card">
    <div class="card-body">
        <form class="row gy-1" wire:submit.prevent="$emit('actividadFiltrar')">
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-eje">Eje de desarrollo</label>
                <select class="form-control" wire:model="eje_id" id="filter-eje">
                    <option selected value="-1">Sin filtros</option>
                    @foreach($ejes as $i => $eje)
                    <option value="{{$i}}">{{$eje}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div wire:ignore>
                    <label class="form-label fw-bolder" for="filter-programa">Programa</label>
                    <select class="form-select" id="filter-programa">
                        @foreach($programas as $i => $programa)
                        <option value="{{$i}}">{{$programa}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div wire:ignore>
                    <label class="form-label fw-bolder" for="filter-estrategia">Estrategia</label>
                    <select class="form-select" id="filter-estrategia">
                        @foreach($estrategias as $i => $estrategia)
                        <option value="{{$i}}">{{$estrategia}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">

                <label class="form-label fw-bolder" for="filter-area">
                    Área
                    @can('sam actividad subareas')
                    @if($subareas)
                    con subáreas
                    @endif
                    @endcan
                </label>
                <div class="row align-items-center">
                    <div class="col-8">
                        <div wire:ignore>
                            <select class="form-control" id="filter-area">
                                @foreach($areas as $i => $area)
                                <option value="{{$i}}">{{$area}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @can('sam actividad subareas')
                    <div class="col d-flex justify-content-end">
                        <div class="form-check form-switch">
                            <input type="checkbox" wire:model.defer="subareas" class="form-check-input">
                        </div>
                    </div>
                    @endcan
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-actividad">Actividad</label>
                <input wire:model.defer="actividad" id="filter-actividad" type="text" class="form-control" />
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-descripcion">Descripcion</label>
                <input wire:model.defer="descripcion" id="filter-descripcion" type="text" class="form-control" />
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filtro-alcance">Alcance</label>
                <input wire:model.defer="alcance" id="filtro-alcance" type="text" class="form-control" />
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-observaciones">Observaciones</label>
                <input wire:model.defer="observaciones" id="filter-observaciones" type="text" name="nombre"
                    class="form-control" />
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-cantidad">Cantidad</label>
                <div class="input-group">
                    <select class="form-select" wire:model.defer="cantidadDir">
                        <option value="">Igual a</option>
                        <option value="1">Mayor a</option>
                        <option value="2">Menor a</option>
                    </select>
                    <input wire:model.defer="cantidad" id="filter-cantidad" type="number" class="form-control" />
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-beneficiarios">Beneficiarios</label>
                <div class="input-group">
                    <select class="form-select" wire:model.defer="beneficiariosDir">
                        <option value="">Igual a</option>
                        <option value="1">Mayor a</option>
                        <option value="2">Menor a</option>
                    </select>
                    <input wire:model.defer="beneficiarios" id="filter-beneficiarios" type="number"
                        class="form-control" />
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-cantidadacomu">Cantidad acomulado</label>
                <div class="input-group">
                    <select class="form-select" wire:model.defer="acomuladoDir">
                        <option value="">Igual a</option>
                        <option value="1">Mayor a</option>
                        <option value="2">Menor a</option>
                    </select>
                    <input wire:model.defer="cantidadAcomulado" id="filter-cantidadacomu" type="number"
                        class="form-control" />
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-bacomulado">Beneficiarios acomulado</label>
                <div class="input-group">
                    <select class="form-select" wire:model.defer="bAcomuladoDir">
                        <option value="">Igual a</option>
                        <option value="1">Mayor a</option>
                        <option value="2">Menor a</option>
                    </select>
                    <input wire:model.defer="beneficiariosAcomulado" id="filter-bacomulado" type="number"
                        class="form-control" />
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-id">Identificador</label>
                <div wire:ignore>
                    <select class="form-control" multiple id="filter-id">

                    </select>
                </div>
            </div>

            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-inicio">Inicio</label>
                <input wire:model.defer="inicio" id="filter-inicio" type="text" class="form-control"
                    placeholder="YYYY-MM-DD" tabindex="1" />
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-fin">Fin</label>
                <input wire:model.defer="fin" id="filter-fin" type="text" class="form-control" placeholder="YYYY-MM-DD"
                    tabindex="1" />
            </div>
            <div class="col-lg-3 col-sm-6">
                <label class="form-label fw-bolder" for="filter-estado">Estado</label>
                <div>
                    <select wire:model.defer="estado" class="form-control" id="filter-estado">
                        <option value="">Sin filtros</option>
                        @foreach($this->estados as $i => $estado)
                        <option value="{{$i}}">{{$estado}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div wire:ignore>
                    <label class="form-label fw-bolder" for="filter-etiquetas">Etiquetas</label>
                    <select class="form-select" id="filter-etiquetas" multiple>
                        @foreach($this->etiquetas as $i => $etiqueta)
                        <option value="{{$i}}">{{$etiqueta}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-lg-9 col-sm-6 justify-content-end d-flex">
                <div class="pt-2">
                    <button type="submit" class="btn btn-primary me-1">Buscar</button>
                    <a class="btn btn-outline-secondary" href="{{route('sam.actividades')}}">
                        Limpiar filtros
                    </a>
                </div>
            </div>

        </form>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        $("#filter-inicio").flatpickr();
        $("#filter-fin").flatpickr();

        let ids = JSON.parse('{!!json_encode($ids)!!}');
        $('#filter-id').select2({
            placeholder: "Sin filtros",
            allowClear: true,
            tags: true,
            data: ids,
        }).val(ids).trigger('change.select2');


        $('#filter-id').on('change', function (e) {
            var data = $('#filter-id').select2("val");
            @this.set('ids', data, true);
        });

        $('#filter-area').select2({
            placeholder: "Sin filtros",
            allowClear: true,
        }).val('{!!$area_id!!}').trigger('change.select2');

        $('#filter-area').on('change', function (e) {
            var data = $('#filter-area').select2("val");
            @this.set('area_id', data, true);
        });

        $('#filter-etiquetas').select2({
            placeholder: "Sin filtros",
        }).val(JSON.parse('{!!json_encode($etiquetas_id)!!}')).trigger('change.select2');

        $('#filter-etiquetas').on('change', function (e) {
            var data = $('#filter-etiquetas').select2("val");
            @this.set('etiquetas_id', data, true);
        });

        $('#filter-programa').select2({
            allowClear: true,
            placeholder: "Sin filtros",
        }).val('{!!$programa_id!!}').trigger('change.select2');

        $('#filter-programa').on('change', function (e) {
            var data = $('#filter-programa').select2("val");
            @this.set('programa_id', data);
        });

        @this.on('setPrograma', (programas, pro) => {
            $('#filter-programa').empty().select2({
                allowClear: true,
                data: programas,
                placeholder: "Sin filtros",
            }).val(pro).trigger('change.select2');
        });

        $('#filter-estrategia').select2({
            allowClear: true,
            placeholder: "Sin filtros",
        }).val('{!!$estrategia_id!!}').trigger('change.select2');

        $('#filter-estrategia').on('change', function (e) {
            var data = $('#filter-estrategia').select2("val");
            @this.set('estrategia_id', data);
        });

        @this.on('setEstrategia', (estrategias, es) => {
            $('#filter-estrategia').empty().select2({
                data: estrategias,
                allowClear: true,
                placeholder: "Sin filtros",
            }).val(es).trigger('change.select2');
        });

    });
</script>
@endpush