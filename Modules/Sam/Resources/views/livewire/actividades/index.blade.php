<div>
    @can("sam actividad estadisticas")
    @livewire('sam::actividades.statistics')
    @endcan
    @include('sam::livewire.actividades.filters')
    <div class="card">
        <div class="card-body">
            <div class="row align-items-center">
                <div class="col">
                    <div class="row row-cols-auto align-items-center gx-1">
                        <div>
                            <label class="form-label fw-bolder mb-0">Exportar: </label>
                        </div>
                        <div>
                            <button wire:click.prevent="exportXLSX()" type="button"
                                class="btn btn-success waves-effect waves-float waves-light"> <i
                                    class="fas fa-file-excel"></i> xlsx</button>
                            @can('sam actividad importar')
                            <button wire:click.prevent="importarXLSX()" type="button"
                                class="btn btn-success waves-effect waves-float waves-light"> <i
                                    class="fas fa-file-excel"></i> Enviar</button>
                            @endcan
                        </div>

                    </div>
                </div>
                <div class="col">
                    <form wire:submit.prevent="$emit('actividadFiltrar')">
                        <div class="row justify-content-end row-cols-auto align-items-center gx-1">
                            <div class="col">
                                <label class="form-label fw-bolder mb-0">Número de filas: </label>
                            </div>
                            <div class="col">
                                <input style="width: 100px" wire:model.defer="num_filas" id="filter-actividad"
                                    type="number" class="form-control" />
                            </div>
                            {{$actividades->links()}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-striped table-sm table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fecha</th>
                        <th>Eje</th>
                        <th>Programa</th>
                        <th>Estrategia</th>
                        <th>Área</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Alcance</th>
                        <th>Parcial</th>
                        <th>Acomulado</th>
                        <th>Observaciones u otros</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($actividades as $index => $actividad)
                    <tr>
                        <td>
                            <span class="fw-bold">{{$actividad->id}}</span>
                        </td>
                        <td>
                            {{$actividad->created_at}}
                        </td>
                        <td>
                            <span class="fw-bold">{{$actividad->eje->nombre}}</span>
                        </td>
                        <td>
                            @if($actividad->programa)
                            <span class="fw-bold">{{$actividad->programa->nombre}}</span>
                            @endif
                        </td>
                        <td>
                            @if($actividad->estrategia)
                            <span class="fw-bold">{{$actividad->estrategia->nombre}}</span>
                            @endif
                        </td>
                        <td>
                            {{$actividad->area->nombre}}
                        </td>
                        <td>
                            {{$actividad->nombre}}
                        </td>
                        <td>
                            {{$actividad->descripcion}}
                        </td>
                        <td>
                            {{$actividad->alcance}}
                        </td>
                        <td>
                            @if($actividad->historico)
                            {{$actividad->unidad->nombre}}: {{intval($actividad->historico->cantidad)}}
                            @endif
                            <div class="text-nowrap">
                                Beneficiarios:
                                @if($actividad->historico)
                                {{intval($actividad->historico->cantidad_beneficiarios)}}
                                @endif
                            </div>
                        </td>
                        <td>
                            {{$actividad->unidad->nombre}}: {{intval($actividad->acomulado)}}
                            <div class="text-nowrap">
                                Beneficiarios:
                                {{intval($actividad->acomulado_beneficiarios)}}
                            </div>
                        </td>
                        <td>
                            @if($actividad->historico)
                            {{$actividad->historico->observaciones}}
                            @endif
                        </td>
                        @if($actividad->historico)
                        <td class="{{$actividad->historico->color}}">
                            <div class="fw-bold">
                                {{$actividad->historico->status}}
                            </div>
                        </td>
                        @else
                        <td></td>
                        @endif
                        <td>
                            <a class="btn btn-sm btn-flat-secondary waves-effect"
                                href="{{route('sam.historicos',['actividad' => $actividad->id])}}">
                                Historico</a>
                            <button data-bs-target="#editActividadModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('actividadEdit','{{ $actividad->id }}')">
                                Editar</button>
                            @can('sam actividad eliminar')
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerDelete','{{ $actividad->id }}')">
                                Eliminar
                            </button>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>