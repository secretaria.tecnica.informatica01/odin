<div class="row">
    @foreach($etiquetas as $etiqueta)
    <div class="col-lg-3 col-sm-6 col-12">
        <a class="card" href="{{route('sam.actividades')}}?ei[0]={{$etiqueta->id}}">
            <div class="card-header">
                <div>
                    <h4 class="fw-bolder">Actividades: {{$etiqueta->actividades_count}}</h2>
                    <p class="card-text">{{$etiqueta->nombre}}</p>
                </div>
                <div class="avatar {{$etiqueta->color}} p-50 m-0">
                    <div class="avatar-content">
                        {!!$etiqueta->imagen!!}
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endforeach
</div>