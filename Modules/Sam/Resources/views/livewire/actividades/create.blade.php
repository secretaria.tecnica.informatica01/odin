<div>
    <div wire:ignore.self class="modal fade" id="addActividadModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Nueva actividad</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">

                        <div class="col-4">
                            <label class="form-label fw-bolder" for="new-eje">Eje de desarrollo</label>
                            <select class="form-control @error('eje_id') is-invalid @enderror" wire:model="eje_id"
                                id="new-eje">
                                <option disabled selected value="-1">Selecione un eje</option>
                                @foreach($ejes as $i => $eje)
                                <option value="{{$i}}">{{$eje}}</option>
                                @endforeach
                            </select>
                            @error('eje_id') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-4">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-programa">Programa</label>
                                <select class="form-select" id="new-programa">
                                    <option disabled selected value="-1">Selecione un programa</option>
                                </select>
                            </div>
                            @error('programa_id') <span class="invalid-feedback d-block">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-4">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-estrategia">Estrategia</label>
                                <select class="form-select" id="new-estrategia">
                                    <option disabled selected value="-1">Selecione una estrategia</option>
                                </select>
                            </div>
                            @error('estrategia_id') <span class="invalid-feedback d-block">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-4">
                            <label class="form-label fw-bolder" for="new-nombre">Nombre</label>
                            <input wire:model="nombre" id="new-nombre" type="text" name="nombre"
                                class="form-control @error('nombre') is-invalid @enderror"
                                placeholder="Ingrese nombre para el área" tabindex="1" />
                            @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-4">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-area">Área</label>
                                <select class="form-select" id="new-area">
                                    <option disabled selected value="-1">Selecione una estrategia</option>
                                    @foreach($areas as $i => $area)
                                    <option value="{{$i}}">{{$area}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('area_id') <span class="invalid-feedback d-block">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-4">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-unidad">Unidad de medida medida</label>
                                <select class="form-select" id="new-unidad">
                                    <option disabled selected value="-1">Selecione una unidad</option>
                                    @foreach($unidades as $i => $unidad)
                                    <option value="{{$i}}">{{$unidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('unidad_id') <span class="invalid-feedback d-block">{{ $message }}</span> @enderror
                        </div>
                       

                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-descripcion">Descripción</label>
                            <textarea rows="4" wire:model="descripcion" id="new-descripcion" type="text"
                                name="descripcion" class="form-control @error('descripcion') is-invalid @enderror"
                                placeholder="Ingrese descripcion para el área" tabindex="1"></textarea>
                            @error('descripcion') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-alcance">Alcance</label>
                            <textarea rows="4" wire:model="alcance" id="new-alcance" type="text" name="alcance"
                                class="form-control @error('alcance') is-invalid @enderror"
                                placeholder="Ingrese alcance para el área" tabindex="1"></textarea>
                            @error('alcance') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <div wire:ignore>
                                <label class="form-label fw-bolder" for="new-etiquetas">Etiquetas</label>
                                <select class="form-select" id="new-etiquetas" multiple>
                                    @foreach($etiquetas as $i => $etiqueta)
                                    <option value="{{$i}}">{{$etiqueta}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('etiquetas_id') <span class="invalid-feedback d-block">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        $('#new-area').select2({
            placeholder: "Selecione un area",
            dropdownParent: $('#addActividadModal')
        });
        $('#new-area').on('change', function (e) {
            var data = $('#new-area').select2("val");
            @this.set('area_id', data);
        });
        $('#new-unidad').select2({
            placeholder: "Selecione un area",
            dropdownParent: $('#addActividadModal')
        });
        $('#new-unidad').on('change', function (e) {
            var data = $('#new-unidad').select2("val");
            @this.set('unidad_id', data);
        });

        $('#new-etiquetas').select2({
            placeholder: "Selecione las etiquetas",
            dropdownParent: $('#addActividadModal')
        });
        
        $('#new-etiquetas').on('change', function (e) {
            var data = $('#new-etiquetas').select2("val");
            @this.set('etiquetas_id', data);
        });

        @this.on('setPrograma', (programas, pro) => {
            console.log(programas);
            $('#new-programa').empty().select2({
                data: programas,
                placeholder: "Selecione un programa",
                dropdownParent: $('#addActividadModal')
            }).val(pro).trigger('change.select2');
        });


        $('#new-programa').on('change', function (e) {
            var data = $('#new-programa').select2("val");
            @this.set('programa_id', data);
        });


        @this.on('setEstrategia', (estrategias, pro) => {
            $('#new-estrategia').empty().select2({
                data: estrategias,
                placeholder: "Selecione una estrategia",
                dropdownParent: $('#addActividadModal')
            }).val(pro).trigger('change.select2');
        });

        $('#new-estrategia').on('change', function (e) {
            var data = $('#new-estrategia').select2("val");
            @this.set('estrategia_id', data);
        });
        @this.on('actividadStored', () => {
            toastr['success']('', 'Actividad guardada', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });

    });
</script>
@endpush