<div>
    <div wire:ignore.self class="modal fade" id="addUnidadModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-sm modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Nueva unidad</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-nombre">Nombre</label>
                            <input wire:model="nombre" id="new-nombre" type="text"
                                class="form-control @error('nombre') is-invalid @enderror"
                                placeholder="Ingrese nombre para el área" tabindex="1" />
                            @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-color">Color</label>
                            <input wire:model="color" id="new-color" type="text"
                                class="form-control @error('color') is-invalid @enderror"
                                placeholder="Ingrese color para el área" tabindex="1" />
                            @error('color') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-descripcion">Descripción</label>
                            <textarea rows="5" wire:model="descripcion" id="new-descripcion" type="text"
                                class="form-control  @error('descripcion') is-invalid @enderror"
                                placeholder="Ingrese un índice para el programa" tabindex="-1"></textarea>
                            @error('descripcion') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                        <div class="d-flex justify-content-between">
                            <label class="form-label fw-bolder" for="old-imagen">Imágen SVG</label>
                             <a target="fontawesome" href="https://fontawesome.com/v5/search">Ejemplos</a>
                            </div>
                            <textarea rows="5" wire:model="imagen" id="new-imagen" type="text"
                                class="form-control  @error('imagen') is-invalid @enderror"
                                placeholder="Ingrese un índice para el programa" tabindex="-1"></textarea>
                            @error('imagen') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {

        @this.on('unidadStored', () => {
            toastr['success']('', 'Etiqueta guardada', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });

    });
</script>
@endpush