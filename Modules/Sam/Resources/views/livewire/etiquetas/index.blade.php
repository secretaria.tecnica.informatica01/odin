<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda">
                    </div>
                </div>
                <div class="col d-flex align-items-center justify-content-end">
                    {{$etiquetas->links()}}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Imágen</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Cantidad usos</th>
                        <th>Fecha de creación</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($etiquetas as $etiqueta)
                    <tr>
                        <td>
                            {{$etiqueta->id}}
                        </td>
                        <td>
                            <div class="avatar {{$etiqueta->color}} p-50 m-0">
                                <div class="avatar-content">
                                    {!!$etiqueta->imagen!!}
                                </div>
                            </div>
                        </td>
                        <td>
                            {{$etiqueta->nombre}}
                        </td>
                        <td>
                            {{$etiqueta->descripcion}}
                        </td>
                        <td>
                            {{$etiqueta->actividades_count}}
                        </td>
                        <td>
                            {{$etiqueta->created_at}}
                        </td>
                        <td>
                            <button data-bs-target="#editUnidadModal" data-bs-toggle="modal" type="button"
                                class="btn btn-sm btn-flat-primary waves-effect"
                                wire:click="$emit('editUnidad','{{ $etiqueta->id }}')">
                                Editar</button>
                            @if($etiqueta->actividades_count == 0)
                            <button class="btn btn-sm btn-flat-danger waves-effect"
                                wire:click="$emit('triggerDelete','{{ $etiqueta->id }}')">
                                Eliminar
                            </button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>