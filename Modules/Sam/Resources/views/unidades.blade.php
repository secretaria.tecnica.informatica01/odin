@extends('layouts.contentLayoutMaster')
@section('title', 'Unidades')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de unidades de médida</h3>
    <p class="mb-2">
    El colaborador es una persona que no necesariamente es un empleado y que se encuentra capacitado para conocer no solo la capacidad de la empresa y sus fortalezas, sino también para mejorar el desempeño de esta.
    </p>
  </div>
  
  <div class="col-auto">
    <button onclick="window.livewire.emit('createUnidad');" click data-bs-target="#addUnidadModal" data-bs-toggle="modal" class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('sam::unidades.index')
@livewire('sam::unidades.create')
@livewire('sam::unidades.edit')
@livewire('sam::unidades.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection