<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(SamController::class)
    ->prefix("sam")
    ->name("sam.")
    ->middleware(["auth"])
    ->group(function () {
        Route::get("/actividades", "actividades")->name("actividades");
        Route::get("/unidades", "unidades")->name("unidades");
        Route::get("/etiquetas", "etiquetas")->name("etiquetas");
        Route::get("/solicitudes", "solicitudes")->name("solicitudes");
        Route::get("/actividades/{actividad}/historicos", "historicos")->name(
            "historicos"
        );
    });
