<?php

namespace Modules\Sam\Entities;

use Modules\Plan\Entities\Eje;
use Modules\Plan\Entities\Programa;
use Modules\Plan\Entities\Estrategia;
use Illuminate\Database\Eloquent\Model;
use Modules\Organizacion\Entities\Area;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Actividad extends Model
{
    use HasFactory;
    protected $table = "actividades";

    protected $fillable = [];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
    public function eje()
    {
        return $this->belongsTo(Eje::class);
    }
    public function programa()
    {
        return $this->belongsTo(Programa::class);
    }
    public function estrategia()
    {
        return $this->belongsTo(Estrategia::class);
    }
    public function unidad()
    {
        return $this->belongsTo(Unidad::class);
    }
    public function historico()
    {
        return $this->hasOne(Historico::class)->latestOfMany();
    }
    public function historicos()
    {
        return $this->hasMany(Historico::class);
    }
    public function etiquetas()
    {
        return $this->belongsToMany(Etiqueta::class);
    }
}
