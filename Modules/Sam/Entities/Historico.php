<?php

namespace Modules\Sam\Entities;

use App\Models\File;
use App\Models\Revision;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Historico extends Model
{
    use HasFactory;

    protected $fillable = ["estado"];

    //estados constantes
    const CONFIRMED = 2;
    const REJECTED = 3;
    const PENDING = 1;
    const DRAFT = 0;

    const ESTADOS = [
        self::DRAFT => "Borrador",
        self::PENDING => "En revisión",
        self::CONFIRMED => "Aprobado",
        self::REJECTED => "Rechazado",
    ];

    const COLORS = [
        self::DRAFT => "table-secondary",
        self::PENDING => "table-info",
        self::CONFIRMED => "table-success",
        self::REJECTED => "table-danger",
    ];

    public function adjuntos()
    {
        return $this->morphToMany(File::class, "adjunto");
    }

    public function scopeDesde($query, $desde)
    {
        if (isset($desde)) {
            return $query->whereDate("inicio", ">=", $desde);
        }
        return $query;
    }
    public function actividad()
    {
        return $this->belongsTo(Actividad::class);
    }
    public function scopeHasta($query, $hasta)
    {
        if (isset($hasta)) {
            return $query->whereDate("inicio", "<=", $hasta);
        }
        return $query;
    }

    public function revisiones()
    {
        return $this->morphMany(Revision::class, "revisable");
    }

    //revisar puede estar mal
    public function revision()
    {
        return $this->morphMany(
            Revision::class,
            "revisable",
            "revisiones"
        )->latest();
    }

    public function getStatusAttribute()
    {
        return array_values(self::ESTADOS)[intval($this->estado)];
    }
    public function getColorAttribute()
    {
        return array_values(self::COLORS)[intval($this->estado)];
    }
}
