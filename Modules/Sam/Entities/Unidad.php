<?php

namespace Modules\Sam\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Unidad extends Model
{
    use HasFactory;
    protected $table = "unidades";

    protected $fillable = ["nombre", "descripcion"];

    public function actividades()
    {
        return $this->hasMany(Actividad::class);
    }
}
