<?php

namespace Modules\Sam\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Etiqueta extends Model
{
    use HasFactory;

    protected $fillable = ["nombre", "descripcion", "color", "imagen"];

    public function actividades()
    {
        return $this->belongsToMany(Actividad::class);
    }
}
