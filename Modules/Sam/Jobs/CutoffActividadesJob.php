<?php

namespace Modules\Sam\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Modules\Sam\Entities\Actividad;
use Modules\Sam\Entities\Historico;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CutoffActividadesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $actividades = Actividad::with("historico")->get();
        DB::beginTransaction();
        foreach ($actividades as $key => $act) {
            $hisNew = new Historico();
            $hisNew->actividad_id = $act->id;
            $hisNew->cantidad = 0;
            $hisNew->cantidad_beneficiarios = 0;
            $hisNew->activo = 1;
            $hisNew->estado = Historico::DRAFT; //modo borrador
            $hisNew->inicio = Carbon::now();
            $hisNew->save();

            if (isset($act->historico)) {
                $his = $act->historico;
                $his->activo = false;
                $his->fin = Carbon::now();
                $his->save();
            }
        }
        DB::commit();
    }
}
