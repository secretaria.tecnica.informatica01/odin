<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("historicos", function (Blueprint $table) {
            $table->id();
            $table
                ->foreignId("actividad_id")
                ->constrained("actividades")
                ->onUpdate("cascade")
                ->onDelete("cascade");
            $table->integer("cantidad_beneficiarios");
            $table->integer("cantidad");
            $table->text("observaciones")->nullable();
            $table->timestamp("inicio")->nullable();
            $table->timestamp("fin")->nullable();
            $table->boolean("activo")->nullable();
            $table->unsignedInteger("estado"); //0:borrador, 1: revision, 2: aceptado: 3 rechazado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("historicos");
    }
};
