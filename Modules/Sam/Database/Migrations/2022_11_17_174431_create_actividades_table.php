<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividades', function (Blueprint $table) {
            $table->id();
            $table->foreignId('estrategia_id')->nullable()->constrained('estrategias');
            $table->foreignId('programa_id')->nullable()->constrained('programas');
            $table->foreignId('eje_id')->nullable()->constrained('ejes');
            $table->foreignId('area_id')->nullable()->constrained('areas');
            $table->foreignId('unidad_id')->constrained('unidades');
            $table->integer('acomulado_beneficiarios')->nullable();
            $table->integer('acomulado')->nullable();
            $table->string('nombre');
            $table->text('descripcion');
            $table->text('alcance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividads');
    }
};
