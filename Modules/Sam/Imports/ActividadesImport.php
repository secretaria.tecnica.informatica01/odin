<?php
namespace Modules\Sam\Imports;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Modules\Plan\Entities\Eje;
use Modules\Sam\Entities\Unidad;
use Illuminate\Support\Collection;
use Modules\Plan\Entities\Programa;
use Modules\Sam\Entities\Actividad;
use Modules\Sam\Entities\Historico;
use Modules\Plan\Entities\Estrategia;
use Modules\Organizacion\Entities\Area;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ActividadesImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
       
        foreach ($rows as $row) 
        {  
           $programa = $this->createPrograma($row);
           $estrategia = $this->createEstrategia($row);
           $area = $this->createArea($row);
           $unidad = $this->createUnidad($row);
           $actividad = $this->createActividad($row,$unidad,$programa,$estrategia, $area);
        }
    }
    private function createPrograma($row){

        $programaNombre = $row['programa'];

        $programaIndice = Str::of($programaNombre)->match('/^\d\.(\d+)\s/');
        $ejeIndice = Str::of($programaNombre)->match('/^(\d+)\./');
        $eje = Eje::where('indice', $ejeIndice)->first();
       
        if(is_null($eje)){
            return;
        }

        //insertar programas
        $programa = $eje->programas()->where('indice', $programaIndice)->first();
        if(is_null($programa)){               
            $programa = new Programa();
            $programaNombre = Str::of($programaNombre)->match('/^\d\.\d+\s(.*)$/');
            $programa->nombre = $programaNombre;
            $programa->indice = $programaIndice;
            $programa->eje_id = $eje->id;
            $programa->save();
        }
        return $programa;
    }

    private function createEstrategia($row){

        $estrategiaNombre = $row['estrategia'];

        $estrategiaIndice = Str::of($estrategiaNombre)->match('/^\d\.\d+\.(\d+)/');
        $programaIndice = Str::of($estrategiaNombre)->match('/^\d\.(\d+)\./');
        $ejeIndice = Str::of($estrategiaNombre)->match('/^(\d+)\./');
        $eje = Eje::where('indice', $ejeIndice)->first();
        
        if(is_null($eje)){
            return;
        }
        
        $programa = $eje->programas()->where('indice', $programaIndice)->first();

        if(is_null($programa)){
            return;
        }
       
        $estrategia = $programa->estrategias()->where('indice', $estrategiaIndice)->first();
        if(is_null($estrategia)){               
            $estrategia = new Estrategia();
            $estrategiaNombre = Str::of($estrategiaNombre)->match('/^\d\.\d+\.\d+[\:\.\-]*(.*)$/');
            $estrategia->nombre = $estrategiaNombre;
            $estrategia->indice = $estrategiaIndice;
            $estrategia->programa_id = $programa->id;
            $estrategia->save();
        }
        return $estrategia->id;
    }

    private function createArea($row){

        $areaNombre = $row['area'];
        $area = Area::where('nombre', $areaNombre)->first();
        if(is_null($area)){
            $area = new Area();
            $area->nombre = $areaNombre;
            $area->save();
        }
        return $area->id;
    }

    private function createUnidad($row){

        $unidadNombre = $row['unidad_de_medida'];
        $unidad = Unidad::where('nombre', $unidadNombre)->first();
        if(is_null($unidad)){
            $unidad = new Unidad();
            $unidad->nombre = $unidadNombre;
            $unidad->descripcion = $unidadNombre;
            $unidad->save();
        }
        return $unidad->id;
    }

    private function createActividad($row, $unidad,$programa,$estrategia, $area){

        $actividadNombre = $row['actividad'];
        $actividad = Actividad::where('nombre', $actividadNombre)->first();
        if(is_null($actividad)){
            $actividad = new Actividad();
            $actividad->area_id = $area;
            $actividad->eje_id = is_null($programa)?null:$programa->eje_id;
            $actividad->programa_id = is_null($programa)?null:$programa->id;
            $actividad->estrategia_id = $estrategia;
            $actividad->nombre = $actividadNombre;
            $actividad->descripcion = $row['descripcion'];
            $actividad->alcance = $row['alcance_beneficio'];
            $actividad->unidad_id = $unidad;
            $actividad->acomulado = intval($row['acumulado_de_la_cantidad']);
            $actividad->acomulado_beneficiarios = intval($row['acumulado_de_beneficiarios']);
            $actividad->save();

            $historico = new Historico();
            $historico->actividad_id = $actividad->id;
            //$historico->fin = $row['fecha_de_ultima_modificacion'];
            $historico->inicio = Carbon::now();
            $historico->observaciones = $row['observaciones_u_otros'];
            $historico->cantidad = intval($row['acumulado_de_la_cantidad']);
            $historico->cantidad_beneficiarios = intval($row['acumulado_de_beneficiarios']);
            $historico->estado = Historico::DRAFT;
            $historico->activo = 1;
            $historico->save();
        }
        return $actividad;
    }
}