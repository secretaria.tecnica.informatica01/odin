<?php

namespace Modules\Sam\Services;

use App\Models\File;
use Illuminate\Http\UploadedFile;
use App\Contracts\UploadFileContract;
use Illuminate\Support\Facades\Storage;

class UploadFileEvidenciaService implements UploadFileContract
{
    public function handle(UploadedFile $file): File
    {
        $name = $file->hashName();

        $folder = "evidencias";

        //default disk
        $disk = config("app.uploads.disk");

        $path = Storage::disk($disk)->putFile($folder, $file);

        $f = new File();
        $f->name = "{$name}";
        $f->file_name = $file->getClientOriginalName();
        $f->mime_type = $file->getClientMimeType();
        $f->path = $path;
        $f->disk = "local";
        $f->size = $file->getSize();

        return $f;
    }
}
