@extends('layouts.contentLayoutMaster')
@section('title', 'Estrategias')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de estrategias</h3>
    <p class="mb-2">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
    </p>
  </div>

  <div class="col-auto">
    <button click data-bs-target="#addEstrategiaModal" data-bs-toggle="modal"
      class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('plan::estrategias.index')
@livewire('plan::estrategias.create')
@livewire('plan::ejes.edit')
@livewire('plan::ejes.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection