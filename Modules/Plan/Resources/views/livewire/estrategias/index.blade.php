<div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-merge">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                        <input wire:model="search" type="search" type="text" class="form-control"
                            placeholder="Búsqueda">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Eje</th>
                        <th>Programa</th>
                        <th>Estrategia</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ejes as $eje)
                    @php($i = 0)
                    @foreach($eje->programas as $j => $programa)
                    @foreach($programa->estrategias as $k => $estrategia)
                    <tr>
                        @if($i==0)
                        <td style="border:solid 2px {{$eje->estilo['bg']}};background: {{$eje->estilo['bg']}}; color: {{$eje->estilo['color']}};"
                            rowspan="{{$eje->estrategias_count}}">
                            <div class="fw-bold">
                                Eje {{$eje->indice}}: {{$eje->nombre}}
                            </div>
                        </td>
                        @endif
                        @if($k==0)
                        <td style="border:solid 2px {{$eje->estilo['bg']}};" rowspan="{{count($programa->estrategias)}}">{{$eje->indice}}.{{$programa->indice}} {{$programa->nombre}}</td>
                        @endif
                        <td style="border:solid 2px {{$eje->estilo['bg']}};">{{$eje->indice}}.{{$programa->indice}}.{{$estrategia->indice}} {{$estrategia->nombre}}</td>
                    </tr>
                    @php($i = 1)
                    @endforeach
                    @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>