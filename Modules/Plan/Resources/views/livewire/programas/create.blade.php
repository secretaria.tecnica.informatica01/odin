<div>
    <div wire:ignore.self class="modal fade" id="addProgramaModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Nuevo programa</h2>
                    </div>
                    <!-- Add role form -->
                    <div class="row gy-1">
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-eje">Eje de desarrollo</label>
                            <select class="form-control @error('eje_id') is-invalid @enderror" wire:model="eje_id"
                                id="new-eje">
                                <option disabled selected value="-1">Selecione un eje</option>
                                @foreach($ejes as $i => $eje)
                                <option value="{{$i}}">{{$eje}}</option>
                                @endforeach
                            </select>
                            @error('eje_id') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-nombre">Nombre del programa</label>
                            <input wire:model="nombre" id="new-nombre" type="text" name="nombre"
                                class="form-control @error('nombre') is-invalid @enderror"
                                placeholder="Ingrese nombre de programa de desarrollo" tabindex="1" />
                            @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12">
                            <label class="form-label fw-bolder" for="new-indice">Índice</label>
                            <input wire:model="indice" id="new-indice" type="text"
                                class="form-control  @error('indice') is-invalid @enderror"
                                placeholder="Ingrese un índice para el programa" tabindex="-1" />
                            @error('indice') <span class="invalid-feedback">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-12 text-center mt-2">
                            <button wire:click.prevent="store()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>
                    <!--/ Add role form -->
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        @this.on('programaStored', () => {
            toastr['success']('', 'Programa guardado', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });
    });
</script>
@endpush