<div>
    <div wire:ignore.self class="modal fade" id="editEjeModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered ">
            <div class="modal-content">
                <div class="modal-header bg-transparent p-0">
                    <button wire:click.prevent="resetInputFields()" type="button" class="btn-close"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-2 pb-2 pt-0">
                    <div class="text-center mb-2">
                        <h2 class="role-title">Edición de eje de desarrollo</h2>
                    </div>

                    <div class="row">

                        <div class="col-6">
                            <!-- Add role form -->
                            <div class="row gy-1">
                                <div class="col-12">
                                    <label class="form-label fw-bolder" for="new-nombre">Nombre de eje de
                                        desarrollo</label>
                                    <input wire:model="nombre" id="new-nombre" type="text" name="nombre"
                                        class="form-control @error('nombre') is-invalid @enderror"
                                        placeholder="Ingrese nombre de eje de desarrollo" tabindex="1" />
                                    @error('nombre') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label fw-bolder" for="new-indice">Índice</label>
                                    <input wire:model="indice" id="new-indice" type="text"
                                        class="form-control  @error('indice') is-invalid @enderror"
                                        placeholder="Ingrese un índice para el eje de desarrollo" tabindex="-1" />
                                    @error('indice') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>
                                <div class="col-12">
                                    <label class="form-label fw-bolder" for="new-objetivo">Objetivo</label>
                                    <textarea rows="5" wire:model="objetivo" id="new-objetivo" type="text"
                                        class="form-control  @error('objetivo') is-invalid @enderror"
                                        placeholder="Ingrese un índice para el eje de desarrollo"
                                        tabindex="-1"></textarea>
                                    @error('objetivo') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>

                            </div>
                            <!--/ Add role form -->
                        </div>
                        <div class="col-6">
                            <!-- Add role form -->
                            <div class="row gy-1">
                                <div class="col-12">
                                    <label class="form-label fw-bolder" for="new-logo">Logotipo</label>
                                    <input wire:model="photo" class="form-control @error('photo') is-invalid @enderror"
                                        type="file" id="formFile" />
                                    @error('photo') <span class="invalid-feedback">{{ $message }}</span> @enderror
                                </div>

                                <div class="col-12">
                                    <div class="text-center">
                                        @if ($photo)
                                        <?php try { $url = $photo->temporaryUrl();?>
                                        <img src="{{ $url }}" class="img-thumbnail">
                                        <?php }catch (Exception $e){} ?>
                                        @elseif($logo_path)
                                        <img src="{{ asset('storage/'.$logo_path) }}" class="img-thumbnail">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!--/ Add role form -->
                        </div>

                        <div class="col-12 text-center mt-3">
                            <button wire:click.prevent="update()" class="btn btn-primary me-1">Guardar</button>
                            <button wire:click.prevent="resetInputFields()" type="reset"
                                class="btn btn-outline-secondary" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    document.addEventListener('livewire:load', function () {
        @this.on('ejeUpdated', () => {
            $('#editEjeModal').modal('hide');
            toastr['info']('', 'Eje actualizado', {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
            });
        });
    });
</script>
@endpush