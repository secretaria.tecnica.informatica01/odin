<div>
    <div class="row">
        @foreach ($ejes as $index => $eje)
        <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-start">
                        <span>Total de programas: {{$eje->permissions_count}}</span>
                    </div>
                    <div class="mt-1 pt-25">
                        <div class="row gx-1">
                            <div class="col-4">
                                <div class="ratio ratio-1x1 img-thumbnail-ratio">
                                    <div style="background-image: url({{ asset('storage/'.$eje->logo_path) }})">
                                    </div>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="role-heading">
                                    <h4 class="fw-bolder">Eje {{$eje->indice}}: {{$eje->nombre}}</h4>
                                </div>
                                <div class="text-truncate">
                                    {{$eje->objetivo}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <button data-bs-target="#editEjeModal" data-bs-toggle="modal" type="button"
                            class="btn btn-sm btn-flat-secondary waves-effect"
                            wire:click="$emit('ejeEdit','{{ $eje->id }}')">Editar</button>
                        <button class="btn btn-sm btn-flat-danger waves-effect"
                            wire:click="$emit('triggerDelete','{{ $eje->id }}')">
                            Eliminar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>