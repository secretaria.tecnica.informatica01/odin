@extends('layouts.contentLayoutMaster')
@section('title', 'Ejes de desarrollo')

@section('vendor-style')
<!-- Vendor css files -->
<link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
<!-- Page css files -->
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/extensions/ext-component-toastr.css')) }}">
@endsection
@section('content')
<div class="row">
  <div class="col">
    <h3>Listado de ejes de desarrollo</h3>
    <p class="mb-2">
      El eje estratégico de desarrollo constituye el principal medio para contribuir al cumplimiento de los objetivos de
      desarrollo prioritarios.
    </p>
  </div>

  <div class="col-auto">
    <button click data-bs-target="#addEjeModal" data-bs-toggle="modal"
      class="btn btn-primary mb-1 waves-effect waves-float waves-light">Nuevo</button>
  </div>
</div>

@livewire('plan::ejes.index')
@livewire('plan::ejes.create')
@livewire('plan::ejes.edit')
@livewire('plan::ejes.delete')

@endsection

@section('vendor-script')
<script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection