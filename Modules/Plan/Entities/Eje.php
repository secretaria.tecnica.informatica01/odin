<?php

namespace Modules\Plan\Entities;

use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Eje extends Model
{
    use HasFactory;
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        "estilo" => "array",
    ];
    protected $appends = ["full_name"];
    protected $fillable = ["nombre", "indice", "objetivo", "logo_path"];

    public $timestamps = false;

    public function delete()
    {
        if ($this->attributes["logo_path"]) {
            $file = $this->attributes["logo_path"];
            $path = Storage::disk("public")->path($file);
            if (File::isFile($path)) {
                \File::delete($path);
            }
        }
        parent::delete();
    }

    public function programas()
    {
        return $this->hasMany(Programa::class)->orderBy('indice');
    }

    public function getFullNameAttribute()
    {
        return "Eje " . $this->indice . ": " . $this->nombre;
    }

    public function estrategias()
    {
        return $this->hasManyThrough(Estrategia::class, Programa::class)->orderBy('indice');
    }
}
