<?php

namespace Modules\Plan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Estrategia extends Model
{
    use HasFactory;

    protected $fillable = [];
    public $timestamps = false;
    
}
