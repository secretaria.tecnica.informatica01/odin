<?php

namespace Modules\Plan\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Programa extends Model
{
    use HasFactory;

    protected $fillable = [];
    public $timestamps = false;
    
    public function estrategias()
    {
        return $this->hasMany(Estrategia::class)->orderBy('indice');
    }
}
