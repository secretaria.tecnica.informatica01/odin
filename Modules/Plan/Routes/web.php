<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(PlanController::class)
    ->prefix("plan")
    ->name("plan.")
    ->middleware(["auth"])
    ->group(function () {
        Route::get("/ejes", "ejes")->name("ejes");
        Route::get("/programas", "programas")->name("programas");
        Route::get("/estrategias", "estrategias")->name("estrategias");
    });
