<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("ejes", function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("indice");
            $table->string("nombre");
            $table->text("objetivo")->nullable();
            $table->string("logo_path")->nullable();
            $table->json("estilo")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("ejes");
    }
};
