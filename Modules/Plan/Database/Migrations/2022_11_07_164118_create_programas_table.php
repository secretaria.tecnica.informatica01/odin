<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("programas", function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("indice");
            $table
                ->foreignId("eje_id")
                ->constrained('ejes')
                ->onUpdate("cascade")
                ->onDelete("cascade");
            $table->text("nombre");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("programas");
    }
};
