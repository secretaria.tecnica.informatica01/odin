<?php

namespace Modules\Plan\Http\Livewire\Estrategias;

use Livewire\Component;
use Modules\Plan\Entities\Eje;
use Illuminate\Validation\Rule;
use Modules\Plan\Entities\Programa;
use Modules\Plan\Entities\Estrategia;

class Create extends Component
{
    public $ejes;
    public $eje_id = -1;
    public $model_id,
        $programa_id = -1,
        $nombre,
        $indice;

    public function render()
    {
        return view("plan::livewire.estrategias.create");
    }

    public function mount()
    {
        $this->ejes = Eje::orderBy("indice")
            ->get()
            ->pluck("full_name", "id")
            ->toArray();
    }

    public function updatedEjeId($value)
    {
        $programas = Programa::where("eje_id", $this->eje_id)
            ->select(["id", "nombre as text"])
            ->get();
        $this->emit("setPrograma", $programas, null);
    }

    protected function rules()
    {
        return [
            "nombre" => [
                "required",
                Rule::unique("programas")->ignore($this->model_id),
            ],
            "indice" => ["numeric", "required"],
            "programa_id" => "exists:programas,id",
        ];
    }

    public function resetInputFields()
    {
        #$this->programa_id = -1;
        $this->model_id = null;
        $this->nombre = "";
        $this->indice = null;
    }

    public function store()
    {
        $this->validate();

        $pro = new Estrategia();
        $pro->programa_id = $this->programa_id;
        $pro->nombre = $this->nombre;
        $pro->indice = $this->indice;
        $pro->save();

        $this->resetInputFields();

        $this->emit("estrategiaStored");
    }
}
