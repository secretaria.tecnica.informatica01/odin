<?php

namespace Modules\Plan\Http\Livewire\Estrategias;

use Livewire\Component;
use Modules\Plan\Entities\Eje;

class Index extends Component
{
    protected $listeners = [
        "estrategiaStored" => '$refresh',
        "estrategiaUpdated" => '$refresh',
        "estrategiaDeleted" => '$refresh',
    ];

    public function render()
    {
        $ejes = Eje::withCount("estrategias")
            ->with("programas", "programas.estrategias")
            ->get();
        return view("plan::livewire.estrategias.index", compact("ejes"));
    }
}
