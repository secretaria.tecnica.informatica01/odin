<?php

namespace Modules\Plan\Http\Livewire\Ejes;

use Livewire\Component;
use Livewire\WithFileUploads;
use Modules\Plan\Entities\Eje;
use Illuminate\Validation\Rule;

class Create extends Component
{
    use WithFileUploads;
    public $photo;
    public $model_id, $nombre, $indice, $objetivo, $logo_path;

    public function render()
    {
        return view("plan::livewire.ejes.create");
    }
    public function updatedPhoto()
    {
        $this->validateOnly("photo");
    }
    protected function rules()
    {
        return [
            "nombre" => [
                "required",
                Rule::unique("ejes")->ignore($this->model_id),
            ],
            "indice" => [
                "numeric",
                "required",
                Rule::unique("ejes")->ignore($this->model_id),
            ],
            "objetivo" => "",
            "photo" => "mimes:jpg,jpeg,png,bmp,gif,svg,webp|max:1024|nullable",
        ];
    }

    public function resetInputFields()
    {
        $this->model_id = null;
        $this->nombre = "";
        $this->indice = null;
        $this->objetivo = null;
        $this->photo = null;
        $this->logo_path = null;
    }

    public function store()
    {
        $this->validate();

        $eje = new Eje();
        $eje->nombre = $this->nombre;
        $eje->indice = $this->indice;
        $eje->objetivo = $this->objetivo;
        $eje->estilo = ["color" => "", "bg" => ""];
        $eje->save();

        if ($this->photo) {
            $eje->logo_path = $this->photo->storeAs(
                "ejes",
                $eje->id . "." . $this->photo->getClientOriginalExtension(),
                "public"
            );
            $eje->save();
        }

        $this->resetInputFields();

        $this->emit("ejeStored");
    }
}
