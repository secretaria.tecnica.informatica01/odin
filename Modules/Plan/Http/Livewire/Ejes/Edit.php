<?php

namespace Modules\Plan\Http\Livewire\Ejes;

use Livewire\Component;
use Modules\Plan\Entities\Eje;

class Edit extends Create
{
    protected $listeners = [
        "ejeEdit" => "edit",
    ];

    public function render()
    {
        return view("plan::livewire.ejes.edit");
    }

    public function edit(Eje $eje)
    {
        $this->model_id = $eje->id;
        $this->nombre = $eje->nombre;
        $this->indice = $eje->indice;
        $this->objetivo = $eje->objetivo;
        $this->logo_path = $eje->logo_path;
    }

    public function update()
    {
        $this->validate();

        $eje = Eje::find($this->model_id);
        $eje->nombre = $this->nombre;
        $eje->indice = $this->indice;
        $eje->objetivo = $this->objetivo;

        $eje->save();

        if ($this->photo) {
            $eje->logo_path = $this->photo->storeAs(
                "ejes",
                $eje->id . "." . $this->photo->getClientOriginalExtension(),
                "public"
            );
            $eje->save();
        }

        $this->resetInputFields();

        $this->emit("ejeUpdated");
    }
}
