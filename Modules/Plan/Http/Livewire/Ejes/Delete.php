<?php

namespace Modules\Plan\Http\Livewire\Ejes;

use Livewire\Component;
use Modules\Plan\Entities\Eje;

class Delete extends Component
{
    public function render()
    {
        return view('plan::livewire.ejes.delete');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($model_id)
    {
        Eje::find($model_id)->delete();
        $this->emit("ejeDeleted");
    }
}
