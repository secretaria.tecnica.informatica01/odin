<?php

namespace Modules\Plan\Http\Livewire\Ejes;

use Livewire\Component;
use Modules\Plan\Entities\Eje;

class Index extends Component
{
    protected $listeners = [
        "ejeStored" => '$refresh',
        "ejeUpdated" => '$refresh',
        "ejeDeleted" => '$refresh',
    ];

    public function render()
    {
        $ejes = Eje::orderBy('indice')->get();
        return view("plan::livewire.ejes.index", compact("ejes"));
    }
}
