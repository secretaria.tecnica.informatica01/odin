<?php

namespace Modules\Plan\Http\Livewire\Programas;

use Livewire\Component;

class Delete extends Component
{
    public function render()
    {
        return view('plan::livewire.programas.delete');
    }
}
