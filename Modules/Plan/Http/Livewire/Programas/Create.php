<?php

namespace Modules\Plan\Http\Livewire\Programas;

use Livewire\Component;
use Modules\Plan\Entities\Eje;
use Illuminate\Validation\Rule;
use Modules\Plan\Entities\Programa;

class Create extends Component
{
    public $ejes;
    public $model_id,
        $eje_id = -1,
        $nombre,
        $indice;

    public function render()
    {
        return view("plan::livewire.programas.create");
    }

    public function mount()
    {
        $this->ejes = Eje::orderBy("indice")
            ->get()
            ->pluck("full_name", "id")
            ->toArray();
    }

    protected function rules()
    {
        return [
            "nombre" => [
                "required",
                Rule::unique("programas")->ignore($this->model_id),
            ],
            "indice" => ["numeric", "required"],
            "eje_id" => "exists:ejes,id",
        ];
    }

    public function resetInputFields()
    {
        $this->eje_id = -1;
        $this->model_id = null;
        $this->nombre = "";
        $this->indice = null;
    }

    public function store()
    {
        $this->validate();

        $pro = new Programa();
        $pro->eje_id = $this->eje_id;
        $pro->nombre = $this->nombre;
        $pro->indice = $this->indice;
        $pro->save();

        $this->resetInputFields();

        $this->emit("programaStored");
    }
}
