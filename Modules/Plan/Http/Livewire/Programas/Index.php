<?php

namespace Modules\Plan\Http\Livewire\Programas;

use Livewire\Component;
use Modules\Plan\Entities\Eje;
use Modules\Plan\Entities\Programa;

class Index extends Component
{
    protected $listeners = [
        "programaStored" => '$refresh',
        "programaUpdated" => '$refresh',
        "programaDeleted" => '$refresh',
    ];

    public function render()
    {
        $ejes = Eje::with("programas")->get();
        return view("plan::livewire.programas.index", compact("ejes"));
    }
}
