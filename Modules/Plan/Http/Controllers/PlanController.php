<?php

namespace Modules\Plan\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PlanController extends Controller
{
    public function ejes()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Ejes de desarrollo"],
            ["name" => "Listado"],
        ];
        return view("plan::ejes",compact('breadcrumbs'));
    }

    public function programas()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Programas"],
            ["name" => "Listado"],
        ];
        return view("plan::programas",compact('breadcrumbs'));
    }

    public function estrategias()
    {
        $breadcrumbs = [
            ["link" => "home", "name" => "Inicio"],
            ["name" => "Estrategias"],
            ["name" => "Listado"],
        ];
        return view("plan::estrategias",compact('breadcrumbs'));
    }
}
