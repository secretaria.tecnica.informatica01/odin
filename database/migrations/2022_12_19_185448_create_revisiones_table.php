<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("revisiones", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("revisable_id");
            $table->string("revisable_type");
            $table
                ->foreignId("colaborador_id")
                ->constrained("colaboradores")
                ->onDelete("cascade");
            $table->unsignedBigInteger("revision_siguiente_id")->nullable();
            $table->unsignedInteger("estado"); //1: revision, 2: aceptado: 3 rechazado
            $table->text("comentarios")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("revisiones");
    }
};
